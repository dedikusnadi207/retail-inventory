<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/popup', function () {
    return view('popup');
});


Route::post('/login','UserController@login');
Route::get('/logout','UserController@logout');

Route::get('/barang/datatables','BarangController@datatables')->name('barang.datatables');
Route::get('/barang/kode','BarangController@kode')->name('barang.kode');
Route::get('/barang/export','BarangController@export')->name('barang.export');

Route::get('/jenis/datatables','JenisController@datatables')->name('jenis.datatables');

Route::get('/satuan/datatables','SatuanController@datatables')->name('satuan.datatables');

Route::get('/merk/datatables','MerkController@datatables')->name('merk.datatables');

Route::get('/supplier/datatables','SupplierController@datatables')->name('supplier.datatables');
Route::get('/supplier/kode','SupplierController@kode')->name('supplier.kode');

Route::get('/user/datatables','UserController@datatables')->name('user.datatables');

Route::get('/masuk/kode','MasukController@kode')->name('masuk.kode');

Route::get('/detailMasuk/{where}/datatables','DetailMasukController@datatables');
Route::get('/detailMasuk/{kd}/{barcode}/scan','DetailMasukController@scan');
Route::delete('/detailMasuk/{kd}/batal','DetailMasukController@batal')->name('detailMasuk.batal');

Route::get('/keluar/kode','KeluarController@kode')->name('keluar.kode');

Route::get('/detailKeluar/{where}/datatables','DetailKeluarController@datatables');
Route::get('/detailKeluar/{kd}/{barcode}/scan','DetailKeluarController@scan');
Route::delete('/detailKeluar/{kd}/batal','DetailKeluarController@batal')->name('detailKeluar.batal');


Route::get('/logAktivitas/datatables','LogAktivitasController@datatables')->name('logAktivitas.datatables');
Route::get('/logPengguna/datatables','LogPenggunaController@datatables')->name('logPengguna.datatables');

Route::get('/recycleBin/{status}','RestoreController@index');
Route::get('/recycleBin/{status}/datatables','RestoreController@datatables');
Route::delete('/recycleBin/{status}/{id}/forceDelete','RestoreController@forceDelete');
Route::get('/recycleBin/{status}/{id}/restore','RestoreController@restore');


// -----------------------------------------------------------------------

// -----------------------------------------------------------------------

Route::group(['middleware'=>'checkLogin'],function()
{
	Route::get('/', function () {
	    return view('layouts.index');
	});
	Route::get('/dashboard', 'UserController@dashboard');
	Route::resource('jenis','JenisController');	
	Route::resource('merk','MerkController');
	Route::resource('satuan','SatuanController');
	Route::resource('barang','BarangController');
	Route::resource('supplier','SupplierController');
	Route::resource('user','UserController');
	Route::resource('masuk','MasukController');
	Route::resource('detailMasuk','DetailMasukController');
	Route::resource('keluar','KeluarController');
	Route::resource('detailKeluar','DetailKeluarController');
	Route::resource('logAktivitas','LogAktivitasController');
	Route::resource('logPengguna','LogPenggunaController');
});
Route::get('getSession',function()
{
	return response()->json(["session"=>session('menu'),"title"=>ucwords(session('menu'))]);
});
Route::get('/ReturnJS',function()
{
	$link = 'includeJS.'.session('menu');
	if (View::exists($link)) {
		return view($link);
	}else{
		return "";
	}
});
Route::get('/lupaPassw',function()
{
	return view('lupaPassw');
});
Route::post('user/changePassw','UserController@changePassword');
Route::post('user/sendPassw','UserController@kirimKode');
Route::post('user/lupaPassw','UserController@lupaPassw');

Route::get('/laporan','LaporanController@index');
Route::post('/laporan/export/masuk','LaporanController@masuk');
Route::post('/laporan/export/keluar','LaporanController@keluar');

Route::get('/logAktivitas/datatables/{menu}','LogAktivitasController@detailMenu');
Route::get('/logAktivitas/table/detailMenu',function()
{
	return view('log.detailMenu');
});
Route::get('/logAktivitas/table/detailLog/{id}','LogAktivitasController@detailLog');


// Auth::routes();

