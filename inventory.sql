-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 05, 2018 at 02:47 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `barcode` varchar(255) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `merk_id` int(11) NOT NULL,
  `jenis_id` int(11) NOT NULL,
  `satuan_id` int(11) NOT NULL,
  `berat` double DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) DEFAULT NULL,
  `stok` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `barcode`, `nama`, `merk_id`, `jenis_id`, `satuan_id`, `berat`, `harga_beli`, `harga_jual`, `stok`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '11111', 'Barang 1', 5, 15, 2, 1, 10000, 20000, 91, '2017-08-07 20:00:17', '2017-08-07 20:00:17', NULL),
(2, '22222', 'Barang 2', 3, 13, 7, 500, 10000, 20000, 1, '2017-08-07 20:47:31', '2017-08-08 19:55:30', NULL),
(3, '33333', 'Barang 3', 5, 14, 7, 500, 60000, 72000, 20, '2017-08-08 20:29:27', '2017-08-08 20:29:27', NULL),
(22, '44444', 'Barang 4', 3, 11, 4, 123, 123, 123, 123, '2017-08-13 23:10:41', '2017-08-14 02:16:32', NULL),
(23, '55555', 'Barang 5', 5, 11, 4, 123, 123, 123, 123, '2017-08-13 23:20:59', '2017-08-13 23:20:59', NULL),
(24, '66666', 'Barang 6', 3, 11, 4, 123, 123, 123, 123, '2017-08-13 23:22:10', '2017-08-13 23:22:10', NULL),
(26, '88888', 'Barang 8', 5, 15, 2, 1, 400, 1100, 10, '2017-08-13 23:28:07', '2017-08-13 23:28:07', NULL),
(27, '99999', 'Barang 9', 3, 11, 4, 123, 500, 1000, 123, '2017-08-14 21:00:58', '2017-10-05 03:13:50', NULL),
(28, '1212121212', 'Barang 12', 5, 14, 4, 0.05, 10000, 20000, 20, '2017-08-15 04:04:30', '2017-10-10 02:32:02', NULL),
(29, '1313131313', 'Barang 13', 3, 11, 4, 0.2, 123, 123123, 124, '2017-08-15 04:16:22', '2017-10-10 02:32:16', NULL),
(31, '1111111111', 'Barang 11', 3, 15, 2, 0.2, 500, 1500, 12, '2017-08-21 06:23:47', '2017-10-10 02:12:42', NULL),
(32, '12312', 'Barang Baru', 3, 15, 4, 1, 1000000, 2000000, 100, '2017-08-22 07:54:28', '2017-10-05 03:15:03', '2017-10-05'),
(33, '1010', '123123', 14, 11, 5, 1, 12, 12, 11, '2017-08-22 09:16:19', '2017-10-05 01:30:30', '2017-10-05'),
(34, '77777', 'Barang 7', 5, 13, 5, 500, 11, 22, 32, '2017-08-22 09:18:50', '2017-10-05 03:14:05', NULL),
(35, '1010101010', 'Barang 10', 14, 8, 4, 1, 500, 1000, 12, '2017-08-30 01:50:41', '2017-10-05 03:14:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `barang_keluar`
--

CREATE TABLE `barang_keluar` (
  `id` int(11) NOT NULL,
  `kd_barang_keluar` varchar(14) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `total_keluar` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_keluar`
--

INSERT INTO `barang_keluar` (`id`, `kd_barang_keluar`, `tanggal`, `total_keluar`, `created_at`, `updated_at`) VALUES
(1, 'KBK20171115001', '2017-11-15', 31, '2017-11-15 13:53:28', '2017-11-15 13:53:28');

-- --------------------------------------------------------

--
-- Table structure for table `barang_masuk`
--

CREATE TABLE `barang_masuk` (
  `id` int(11) NOT NULL,
  `kd_barang_masuk` varchar(14) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `total_masuk` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang_masuk`
--

INSERT INTO `barang_masuk` (`id`, `kd_barang_masuk`, `tanggal`, `supplier_id`, `total_masuk`, `created_at`, `updated_at`) VALUES
(1, 'KBM20171115001', '2017-11-15', 1, 2, '2017-11-15 13:52:25', '2017-11-15 13:52:25');

-- --------------------------------------------------------

--
-- Table structure for table `detail_barang_keluar`
--

CREATE TABLE `detail_barang_keluar` (
  `id` int(11) NOT NULL,
  `kd_barang_keluar` varchar(14) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_barang_keluar`
--

INSERT INTO `detail_barang_keluar` (`id`, `kd_barang_keluar`, `barang_id`, `jumlah`) VALUES
(1, 'KBK20171115001', 1, 11),
(2, 'KBK20171115001', 2, 20);

--
-- Triggers `detail_barang_keluar`
--
DELIMITER $$
CREATE TRIGGER `delete_stok_keluar` AFTER DELETE ON `detail_barang_keluar` FOR EACH ROW BEGIN
	UPDATE barang SET barang.stok = barang.stok + OLD.jumlah WHERE barang.id = OLD.barang_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert_stok_keluar` AFTER INSERT ON `detail_barang_keluar` FOR EACH ROW BEGIN
	UPDATE barang SET barang.stok = barang.stok - NEW.jumlah WHERE barang.id = NEW.barang_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_stok_keluar` AFTER UPDATE ON `detail_barang_keluar` FOR EACH ROW BEGIN
	UPDATE barang SET barang.stok = barang.stok + OLD.jumlah - NEW.jumlah WHERE barang.id = NEW.barang_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `detail_barang_masuk`
--

CREATE TABLE `detail_barang_masuk` (
  `id` int(11) NOT NULL,
  `kd_barang_masuk` varchar(14) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_barang_masuk`
--

INSERT INTO `detail_barang_masuk` (`id`, `kd_barang_masuk`, `barang_id`, `jumlah`) VALUES
(1, 'KBM20171115001', 1, 1),
(2, 'KBM20171115001', 2, 1),
(3, 'KBM20180105001', 1, 1);

--
-- Triggers `detail_barang_masuk`
--
DELIMITER $$
CREATE TRIGGER `delete_stok_masuk` AFTER DELETE ON `detail_barang_masuk` FOR EACH ROW BEGIN
	UPDATE barang SET barang.stok = barang.stok - OLD.jumlah WHERE barang.id = OLD.barang_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `insert_stok_masuk` AFTER INSERT ON `detail_barang_masuk` FOR EACH ROW BEGIN
	UPDATE barang SET barang.stok = barang.stok + NEW.jumlah WHERE barang.id = NEW.barang_id;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_stok_masuk` AFTER UPDATE ON `detail_barang_masuk` FOR EACH ROW BEGIN
	UPDATE barang SET barang.stok = barang.stok - OLD.jumlah + NEW.jumlah WHERE barang.id = NEW.barang_id;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id`, `nama`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Susu', '2017-07-27 03:00:12', '2017-07-31 00:50:27', NULL),
(5, 'Minuman', '2017-07-27 03:07:14', '2017-08-04 01:11:10', NULL),
(6, 'Pakaian', '2017-07-27 03:08:04', '2017-07-28 02:13:38', NULL),
(7, 'Sepatu', '2017-07-27 03:08:04', '2017-08-04 01:10:50', NULL),
(8, 'Perhiasan', '2017-07-27 03:08:04', '2017-08-04 01:11:01', NULL),
(9, 'Teh', '2017-07-27 03:09:04', '2017-07-28 02:12:27', NULL),
(10, 'Kompor', '2017-07-27 03:09:04', '2017-08-04 01:11:19', NULL),
(11, 'Baju', '2017-07-27 03:09:42', '2017-08-22 10:10:18', NULL),
(12, 'Tepung', '2017-07-27 03:09:42', '2017-08-02 23:38:50', NULL),
(13, 'Kaus kaki', '2017-07-27 03:10:02', '2017-07-28 02:12:09', NULL),
(14, 'Kaus', '2017-07-26 20:30:48', '2017-08-22 10:06:19', NULL),
(15, 'Elektronik', '2017-08-09 02:57:38', '2017-08-09 02:57:38', NULL),
(16, 'Update Jenis', '2017-07-26 20:51:11', '2017-08-16 07:22:40', '2017-08-16'),
(17, 'Jenis tes', '2017-07-26 20:52:32', '2017-08-16 07:22:52', '2017-08-16'),
(18, 'Lampu', '2017-07-28 02:12:43', '2017-07-28 02:12:43', NULL),
(20, 'Update Ini Jenis', '2017-08-02 22:51:11', '2017-08-16 07:22:29', '2017-08-16'),
(21, 'update jenisLog', '2017-10-02 07:29:04', '2017-10-02 07:29:23', '2017-10-02');

-- --------------------------------------------------------

--
-- Table structure for table `log_aktivitas`
--

CREATE TABLE `log_aktivitas` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `data_new` text,
  `data_old` text,
  `data_change` text,
  `aksi` varchar(255) NOT NULL,
  `menu` varchar(13) NOT NULL,
  `aktivitas` varchar(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_aktivitas`
--

INSERT INTO `log_aktivitas` (`id`, `user_id`, `data_new`, `data_old`, `data_change`, `aksi`, `menu`, `aktivitas`, `created_at`, `updated_at`) VALUES
(1, 4, NULL, '{\"Nama\":\"asdas\"}', NULL, 'Berhasil Menghapus Merk asdas', 'Merk', 'D', '2017-10-05 01:36:23', '2017-10-05 01:36:23'),
(2, 4, NULL, '{\"Nama\":\"asdas\"}', NULL, 'Berhasil Menghapus merk asdas secara permanen', 'Tempat Sampah', 'D', '2017-10-05 01:36:33', '2017-10-05 01:36:33'),
(3, 4, NULL, '{\"Nama\":\"update merkLog\"}', NULL, 'Berhasil Menghapus merk update merkLog secara permanen', 'Tempat Sampah', 'D', '2017-10-05 01:36:40', '2017-10-05 01:36:40'),
(4, 4, '{\"Barcode\":\"99\",\"Nama\":\"a 99\",\"Merk\":\"Compaq\",\"Jenis\":\"Elektronik\",\"Satuan\":\"Dus\",\"Berat\":\"123 kg\",\"Harga Beli\":500,\"Harga Jual\":1500,\"Stok\":12}', NULL, NULL, 'Berhasil Mengembalikan barang a 99', 'Tempat Sampah', 'C', '2017-10-05 01:40:15', '2017-10-05 01:40:15'),
(5, 1, '{\"Barcode\":\"99\",\"Nama\":\"Barang 11\",\"Merk\":\"Compaq\",\"Jenis\":\"Elektronik\",\"Satuan\":\"Dus\",\"Berat\":\"123 kg\",\"Harga Beli\":\"500\",\"Harga Jual\":\"1500\",\"Stok\":\"12\"}', '{\"Barcode\":\"99\",\"Nama\":\"a 99\",\"Merk\":\"Compaq\",\"Jenis\":\"Elektronik\",\"Satuan\":\"Dus\",\"Berat\":\"123 kg\",\"Harga Beli\":500,\"Harga Jual\":1500,\"Stok\":12}', '{\"Nama\":\"Barang 11\"}', 'Berhasil Mengubah Barang Barang 11', 'Barang', 'U', '2017-10-05 03:12:14', '2017-10-05 03:12:14'),
(6, 1, '{\"Barcode\":\"11011\",\"Nama\":\"Barang 2\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus kaki\",\"Satuan\":\"Box\",\"Berat\":\"500 gr\",\"Harga Beli\":\"11\",\"Harga Jual\":\"22\",\"Stok\":\"32\"}', '{\"Barcode\":\"11011\",\"Nama\":\"asdadw\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus kaki\",\"Satuan\":\"Box\",\"Berat\":\"500 gr\",\"Harga Beli\":11,\"Harga Jual\":22,\"Stok\":32}', '{\"Nama\":\"Barang 2\"}', 'Berhasil Mengubah Barang Barang 2', 'Barang', 'U', '2017-10-05 03:12:28', '2017-10-05 03:12:28'),
(7, 1, '{\"Barcode\":\"909090\",\"Nama\":\"Barang 10\",\"Merk\":\"ASUS\",\"Jenis\":\"Perhiasan\",\"Satuan\":\"Buah\",\"Berat\":\"1 kg\",\"Harga Beli\":\"500\",\"Harga Jual\":\"1000\",\"Stok\":\"12\"}', '{\"Barcode\":\"909090\",\"Nama\":\"Ini Barang\",\"Merk\":\"ASUS\",\"Jenis\":\"Perhiasan\",\"Satuan\":\"Buah\",\"Berat\":\"1 kg\",\"Harga Beli\":500,\"Harga Jual\":1000,\"Stok\":12}', '{\"Nama\":\"Barang 10\"}', 'Berhasil Mengubah Barang Barang 10', 'Barang', 'U', '2017-10-05 03:12:59', '2017-10-05 03:12:59'),
(8, 1, '{\"Barcode\":\"10101\",\"Nama\":\"Barang 12\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus\",\"Satuan\":\"Buah\",\"Berat\":\"500 gr\",\"Harga Beli\":\"10000\",\"Harga Jual\":\"20000\",\"Stok\":\"20\"}', '{\"Barcode\":\"10101\",\"Nama\":\"Barang 101\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus\",\"Satuan\":\"Buah\",\"Berat\":\"500 gr\",\"Harga Beli\":10000,\"Harga Jual\":20000,\"Stok\":20}', '{\"Nama\":\"Barang 12\"}', 'Berhasil Mengubah Barang Barang 12', 'Barang', 'U', '2017-10-05 03:13:15', '2017-10-05 03:13:15'),
(9, 1, '{\"Barcode\":\"111111\",\"Nama\":\"Barang 13\",\"Merk\":\"Compaq\",\"Jenis\":\"Baju\",\"Satuan\":\"Buah\",\"Berat\":\"123\",\"Harga Beli\":\"123\",\"Harga Jual\":\"123123\",\"Stok\":\"124\"}', '{\"Barcode\":\"111111\",\"Nama\":\"Barang 111\",\"Merk\":\"Compaq\",\"Jenis\":\"Baju\",\"Satuan\":\"Buah\",\"Berat\":\"123\",\"Harga Beli\":123,\"Harga Jual\":123123,\"Stok\":124}', '{\"Nama\":\"Barang 13\"}', 'Berhasil Mengubah Barang Barang 13', 'Barang', 'U', '2017-10-05 03:13:28', '2017-10-05 03:13:28'),
(10, 1, '{\"Barcode\":\"99999\",\"Nama\":\"Barang 9\",\"Merk\":\"Compaq\",\"Jenis\":\"Baju\",\"Satuan\":\"Buah\",\"Berat\":\"123\",\"Harga Beli\":\"500\",\"Harga Jual\":\"1000\",\"Stok\":\"123\"}', '{\"Barcode\":\"99999\",\"Nama\":\"Barang 99\",\"Merk\":\"Compaq\",\"Jenis\":\"Baju\",\"Satuan\":\"Buah\",\"Berat\":\"123\",\"Harga Beli\":500,\"Harga Jual\":1000,\"Stok\":123}', '{\"Nama\":\"Barang 9\"}', 'Berhasil Mengubah Barang Barang 9', 'Barang', 'U', '2017-10-05 03:13:50', '2017-10-05 03:13:50'),
(11, 1, '{\"Barcode\":\"77777\",\"Nama\":\"Barang 7\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus kaki\",\"Satuan\":\"Box\",\"Berat\":\"500 gr\",\"Harga Beli\":\"11\",\"Harga Jual\":\"22\",\"Stok\":\"32\"}', '{\"Barcode\":\"11011\",\"Nama\":\"Barang 2\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus kaki\",\"Satuan\":\"Box\",\"Berat\":\"500 gr\",\"Harga Beli\":11,\"Harga Jual\":22,\"Stok\":32}', '{\"Barcode\":\"77777\",\"Nama\":\"Barang 7\"}', 'Berhasil Mengubah Barang Barang 7', 'Barang', 'U', '2017-10-05 03:14:05', '2017-10-05 03:14:05'),
(12, 1, '{\"Barcode\":\"1010101010\",\"Nama\":\"Barang 10\",\"Merk\":\"ASUS\",\"Jenis\":\"Perhiasan\",\"Satuan\":\"Buah\",\"Berat\":\"1 kg\",\"Harga Beli\":\"500\",\"Harga Jual\":\"1000\",\"Stok\":\"12\"}', '{\"Barcode\":\"909090\",\"Nama\":\"Barang 10\",\"Merk\":\"ASUS\",\"Jenis\":\"Perhiasan\",\"Satuan\":\"Buah\",\"Berat\":\"1 kg\",\"Harga Beli\":500,\"Harga Jual\":1000,\"Stok\":12}', '{\"Barcode\":\"1010101010\"}', 'Berhasil Mengubah Barang Barang 10', 'Barang', 'U', '2017-10-05 03:14:18', '2017-10-05 03:14:18'),
(13, 1, '{\"Barcode\":\"1111111111\",\"Nama\":\"Barang 11\",\"Merk\":\"Compaq\",\"Jenis\":\"Elektronik\",\"Satuan\":\"Dus\",\"Berat\":\"123 kg\",\"Harga Beli\":\"500\",\"Harga Jual\":\"1500\",\"Stok\":\"12\"}', '{\"Barcode\":\"99\",\"Nama\":\"Barang 11\",\"Merk\":\"Compaq\",\"Jenis\":\"Elektronik\",\"Satuan\":\"Dus\",\"Berat\":\"123 kg\",\"Harga Beli\":500,\"Harga Jual\":1500,\"Stok\":12}', '{\"Barcode\":\"1111111111\"}', 'Berhasil Mengubah Barang Barang 11', 'Barang', 'U', '2017-10-05 03:14:29', '2017-10-05 03:14:29'),
(14, 1, '{\"Barcode\":\"1212121212\",\"Nama\":\"Barang 12\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus\",\"Satuan\":\"Buah\",\"Berat\":\"500 gr\",\"Harga Beli\":\"10000\",\"Harga Jual\":\"20000\",\"Stok\":\"20\"}', '{\"Barcode\":\"10101\",\"Nama\":\"Barang 12\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus\",\"Satuan\":\"Buah\",\"Berat\":\"500 gr\",\"Harga Beli\":10000,\"Harga Jual\":20000,\"Stok\":20}', '{\"Barcode\":\"1212121212\"}', 'Berhasil Mengubah Barang Barang 12', 'Barang', 'U', '2017-10-05 03:14:39', '2017-10-05 03:14:39'),
(15, 1, '{\"Barcode\":\"1313131313\",\"Nama\":\"Barang 13\",\"Merk\":\"Compaq\",\"Jenis\":\"Baju\",\"Satuan\":\"Buah\",\"Berat\":\"123\",\"Harga Beli\":\"123\",\"Harga Jual\":\"123123\",\"Stok\":\"124\"}', '{\"Barcode\":\"111111\",\"Nama\":\"Barang 13\",\"Merk\":\"Compaq\",\"Jenis\":\"Baju\",\"Satuan\":\"Buah\",\"Berat\":\"123\",\"Harga Beli\":123,\"Harga Jual\":123123,\"Stok\":124}', '{\"Barcode\":\"1313131313\"}', 'Berhasil Mengubah Barang Barang 13', 'Barang', 'U', '2017-10-05 03:14:50', '2017-10-05 03:14:50'),
(16, 1, NULL, '{\"Barcode\":\"12312\",\"Nama\":\"Barang Baru\",\"Merk\":\"Compaq\",\"Jenis\":\"Elektronik\",\"Satuan\":\"Buah\",\"Berat\":\"1 kg\",\"Harga Beli\":1000000,\"Harga Jual\":2000000,\"Stok\":100}', NULL, 'Berhasil Menghapus Barang Barang Baru', 'Barang', 'D', '2017-10-05 03:15:03', '2017-10-05 03:15:03'),
(17, 1, '{\"Nama\":\"Merk 1\"}', '{\"Nama\":\"asdas\"}', '{\"Nama\":\"Merk 1\"}', 'Berhasil Mengubah Merk Merk 1', 'Merk', 'U', '2017-10-05 03:15:21', '2017-10-05 03:15:21'),
(18, 1, '{\"Nama\":\"Merk 2\"}', '{\"Nama\":\"asdf\"}', '{\"Nama\":\"Merk 2\"}', 'Berhasil Mengubah Merk Merk 2', 'Merk', 'U', '2017-10-05 03:15:33', '2017-10-05 03:15:33'),
(19, 1, '{\"Nama\":\"Merk 3\"}', '{\"Nama\":\"asdf\"}', '{\"Nama\":\"Merk 3\"}', 'Berhasil Mengubah Merk Merk 3', 'Merk', 'U', '2017-10-05 03:15:46', '2017-10-05 03:15:46'),
(20, 1, NULL, '{\"Nama\":\"Samsung\"}', NULL, 'Berhasil Menghapus Merk Samsung', 'Merk', 'D', '2017-10-05 03:15:58', '2017-10-05 03:15:58'),
(21, 1, NULL, '{\"Nama\":\"x3\"}', NULL, 'Berhasil Menghapus Merk x3', 'Merk', 'D', '2017-10-05 03:16:05', '2017-10-05 03:16:05'),
(22, 1, NULL, '{\"Nama\":\"x2\"}', NULL, 'Berhasil Menghapus Merk x2', 'Merk', 'D', '2017-10-05 03:16:10', '2017-10-05 03:16:10'),
(23, 1, NULL, '{\"Nama\":\"coba Log\"}', NULL, 'Berhasil Menghapus Merk coba Log', 'Merk', 'D', '2017-10-05 03:16:16', '2017-10-05 03:16:16'),
(24, 1, NULL, '{\"Nama\":\"coba\"}', NULL, 'Berhasil Menghapus Merk coba', 'Merk', 'D', '2017-10-05 03:16:21', '2017-10-05 03:16:21'),
(25, 1, NULL, '{\"Nama\":\"Coba Lagi\"}', NULL, 'Berhasil Menghapus Merk Coba Lagi', 'Merk', 'D', '2017-10-05 03:16:25', '2017-10-05 03:16:25'),
(26, 1, NULL, '{\"Nama\":\"Coba Update\"}', NULL, 'Berhasil Menghapus Merk Coba Update', 'Merk', 'D', '2017-10-05 03:16:28', '2017-10-05 03:16:28'),
(27, 1, NULL, '{\"Nama\":\"newMerk\"}', NULL, 'Berhasil Menghapus Merk newMerk', 'Merk', 'D', '2017-10-05 03:16:34', '2017-10-05 03:16:34'),
(28, 1, NULL, '{\"Nama\":\"Merk Baru\"}', NULL, 'Berhasil Menghapus Merk Merk Baru', 'Merk', 'D', '2017-10-05 03:57:11', '2017-10-05 03:57:11'),
(29, 1, '{\"Barcode\":\"11111\",\"Nama\":\"Tes\",\"Merk\":\"Merk 1\",\"Jenis\":\"Kaus kaki\",\"Satuan\":\"Lusin\",\"Berat\":\"35 gr\",\"Harga Beli\":\"10000\",\"Harga Jual\":\"20000\",\"Stok\":\"20\"}', NULL, NULL, 'Berhasil Menambahkan Barang Tes', 'Barang', 'C', '2017-10-06 02:49:59', '2017-10-06 02:49:59'),
(30, 1, '{\"Barcode\":\"11111\",\"Nama\":\"ASD\",\"Merk\":\"Merk 1\",\"Jenis\":\"Perhiasan\",\"Satuan\":\"Buah\",\"Berat\":\"10\",\"Harga Beli\":\"20\",\"Harga Jual\":\"20\",\"Stok\":\"10\"}', NULL, NULL, 'Berhasil Menambahkan Barang ASD', 'Barang', 'C', '2017-10-06 03:03:39', '2017-10-06 03:03:39'),
(31, 1, '{\"Username\":\"admin\",\"Nama\":\"Dedi\",\"Nomor Telepon\":\"080808080808\",\"Email\":\"dedikusnadi207@gmail.com\",\"Alamat\":\"Kp. Caringin Desa Banjarsari Kec. Ciawi Kab.Bogor Jawa Barat\"}', '{\"Username\":\"admin\",\"Nama\":\"Dedi\",\"Nomor Telepon\":\"080808080808\",\"Email\":\"dedikusnadi207@gmail.com\",\"Alamat\":\"Kp. Caringin Desa Banjarsari Kec. Ciawi Kab.Bogor Jawa Barat\"}', '[]', 'Berhasil Mengubah User Dedi', 'User', 'U', '2017-10-06 03:05:24', '2017-10-06 03:05:24'),
(32, 1, '{\"Username\":\"admin\",\"Nama\":\"Dedi Kusnadi\",\"Nomor Telepon\":\"082210734711\",\"Email\":\"dedikusnadi207@gmail.com\",\"Alamat\":\"Kp. Caringin Desa Banjarsari Kec. Ciawi Kab.Bogor Jawa Barat\"}', '{\"Username\":\"admin\",\"Nama\":\"Dedi\",\"Nomor Telepon\":\"080808080808\",\"Email\":\"dedikusnadi207@gmail.com\",\"Alamat\":\"Kp. Caringin Desa Banjarsari Kec. Ciawi Kab.Bogor Jawa Barat\"}', '{\"Nama\":\"Dedi Kusnadi\",\"Nomor Telepon\":\"082210734711\"}', 'Berhasil Mengubah User Dedi Kusnadi', 'User', 'U', '2017-10-06 03:06:24', '2017-10-06 03:06:24'),
(33, 1, '{\"Barcode\":\"1111111111\",\"Nama\":\"Barang 11\",\"Merk\":\"Compaq\",\"Jenis\":\"Elektronik\",\"Satuan\":\"Dus\",\"Berat\":\"0.2\",\"Harga Beli\":\"500\",\"Harga Jual\":\"1500\",\"Stok\":\"12\"}', '{\"Barcode\":\"1111111111\",\"Nama\":\"Barang 11\",\"Merk\":\"Compaq\",\"Jenis\":\"Elektronik\",\"Satuan\":\"Dus\",\"Berat\":123,\"Harga Beli\":500,\"Harga Jual\":1500,\"Stok\":12}', '{\"Berat\":\"0.2\"}', 'Berhasil Mengubah Barang Barang 11', 'Barang', 'U', '2017-10-10 02:12:42', '2017-10-10 02:12:42'),
(34, 1, '{\"Barcode\":\"1212121212\",\"Nama\":\"Barang 12\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus\",\"Satuan\":\"Buah\",\"Berat\":\"0.05\",\"Harga Beli\":\"10000\",\"Harga Jual\":\"20000\",\"Stok\":\"20\"}', '{\"Barcode\":\"1212121212\",\"Nama\":\"Barang 12\",\"Merk\":\"Ikea\",\"Jenis\":\"Kaus\",\"Satuan\":\"Buah\",\"Berat\":500,\"Harga Beli\":10000,\"Harga Jual\":20000,\"Stok\":20}', '{\"Berat\":\"0.05\"}', 'Berhasil Mengubah Barang Barang 12', 'Barang', 'U', '2017-10-10 02:32:02', '2017-10-10 02:32:02'),
(35, 1, '{\"Barcode\":\"1313131313\",\"Nama\":\"Barang 13\",\"Merk\":\"Compaq\",\"Jenis\":\"Baju\",\"Satuan\":\"Buah\",\"Berat\":\"0.2\",\"Harga Beli\":\"123\",\"Harga Jual\":\"123123\",\"Stok\":\"124\"}', '{\"Barcode\":\"1313131313\",\"Nama\":\"Barang 13\",\"Merk\":\"Compaq\",\"Jenis\":\"Baju\",\"Satuan\":\"Buah\",\"Berat\":123,\"Harga Beli\":123,\"Harga Jual\":123123,\"Stok\":124}', '{\"Berat\":\"0.2\"}', 'Berhasil Mengubah Barang Barang 13', 'Barang', 'U', '2017-10-10 02:32:16', '2017-10-10 02:32:16'),
(36, 1, '{\"Nama\":\"ASD\"}', NULL, NULL, 'Berhasil Menambahkan Satuan ASD', 'Satuan', 'C', '2018-01-05 07:02:17', '2018-01-05 07:02:17'),
(37, 1, '{\"Nama\":\"ASD\"}', NULL, NULL, 'Berhasil Menambahkan Satuan ASD', 'Satuan', 'C', '2018-01-05 07:02:25', '2018-01-05 07:02:25'),
(38, 1, '{\"Kode Supplier\":\"KS0004\",\"Nama\":\"Dedi\",\"Nomor Telepon\":\"97987\",\"Email\":\"dedikusnadi208@gmail.com\",\"Alamat\":\"Dimana-mana\"}', NULL, NULL, 'Berhasil Menambahkan Supplier Dedi', 'Supplier', 'C', '2018-01-05 07:02:54', '2018-01-05 07:02:54');

-- --------------------------------------------------------

--
-- Table structure for table `log_pengguna`
--

CREATE TABLE `log_pengguna` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `status` varchar(11) NOT NULL,
  `logged_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logged_out` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `merk`
--

CREATE TABLE `merk` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `merk`
--

INSERT INTO `merk` (`id`, `nama`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Sony', '2017-07-27 09:06:56', '2017-08-09 00:54:32', NULL),
(2, 'Samsung', '2017-07-27 09:06:56', '2017-07-27 09:06:56', NULL),
(3, 'Compaq', '2017-07-27 09:07:51', '2017-08-23 08:52:50', NULL),
(4, 'Toshiba', '2017-07-27 09:07:51', '2017-07-27 09:07:51', NULL),
(5, 'Ikea', '2017-07-27 09:07:51', '2017-07-27 09:07:51', NULL),
(6, 'Tupperware', '2017-07-27 09:07:51', '2017-07-27 09:07:51', NULL),
(14, 'ASUS', '2017-08-22 07:18:25', '2017-10-02 07:05:40', NULL),
(17, 'Merk Baru', '2017-08-23 08:11:49', '2017-10-03 13:16:46', NULL),
(18, 'newMerk', '2017-10-02 03:38:43', '2017-10-05 03:16:34', '2017-10-05'),
(19, 'coba', '2017-10-02 03:46:08', '2017-10-05 03:16:21', '2017-10-05'),
(20, 'Coba Update', '2017-10-02 03:46:20', '2017-10-05 03:16:28', '2017-10-05'),
(21, 'Coba Lagi', '2017-10-02 03:52:40', '2017-10-05 03:16:25', '2017-10-05'),
(23, 'Merk 1', '2017-10-02 03:54:27', '2017-10-05 03:15:21', NULL),
(24, 'Merk 2', '2017-10-02 03:54:46', '2017-10-05 03:15:33', NULL),
(25, 'Merk 3', '2017-10-02 03:54:46', '2017-10-05 03:15:46', NULL),
(30, 'Samsung', '2017-10-02 04:01:53', '2017-10-05 03:15:58', '2017-10-05'),
(31, 'x2', '2017-10-02 04:04:09', '2017-10-05 03:16:10', '2017-10-05'),
(32, 'x3', '2017-10-02 04:18:58', '2017-10-05 03:16:05', '2017-10-05'),
(34, 'coba Log', '2017-10-02 06:45:45', '2017-10-05 03:16:16', '2017-10-05'),
(36, 'Merk Baru', '2017-10-03 13:01:48', '2017-10-05 03:57:11', '2017-10-05');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('dedikusnadi207@gmail.com', '$2y$10$hH4XCBNZCIDBuaswWcFodOjIGQjr2cznnrdohrOHVs/x.Vgo1CEZy', '2017-08-07 01:02:17');

-- --------------------------------------------------------

--
-- Stand-in structure for view `qw_laporan_keluar`
-- (See below for the actual view)
--
CREATE TABLE `qw_laporan_keluar` (
`kd_barang_keluar` varchar(14)
,`tanggal` date
,`barcode` varchar(255)
,`nama_barang` varchar(45)
,`nama_merk` varchar(45)
,`nama_jenis` varchar(45)
,`nama_satuan` varchar(45)
,`berat` double
,`jumlah` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `qw_laporan_masuk`
-- (See below for the actual view)
--
CREATE TABLE `qw_laporan_masuk` (
`kd_barang_masuk` varchar(14)
,`tanggal` date
,`nama_supplier` varchar(50)
,`barcode` varchar(255)
,`nama_barang` varchar(45)
,`nama_merk` varchar(45)
,`nama_jenis` varchar(45)
,`nama_satuan` varchar(45)
,`berat` double
,`jumlah` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id`, `nama`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Pack', '2017-07-27 07:01:27', NULL, NULL),
(2, 'Dus', '2017-07-27 07:01:27', NULL, NULL),
(3, 'Sachet', '2017-07-27 07:02:57', NULL, NULL),
(4, 'Buah', '2017-07-27 07:02:57', NULL, NULL),
(5, 'Box', '2017-07-27 07:02:57', '2017-08-16 07:46:23', NULL),
(6, 'Unit', '2017-07-27 07:02:57', NULL, NULL),
(7, 'Lusin', '2017-07-27 07:02:57', NULL, NULL),
(8, 'AAA', '2017-08-22 09:10:15', '2017-08-29 04:11:48', '2017-08-29'),
(9, 'AABC', '2017-08-22 09:10:20', '2017-08-22 09:10:31', '2017-08-22'),
(10, 'update satuanLog', '2017-10-02 07:30:48', '2017-10-03 02:29:57', '2017-10-03'),
(11, 'ASD', '2018-01-05 07:02:17', '2018-01-05 07:02:17', NULL),
(12, 'ASD', '2018-01-05 07:02:25', '2018-01-05 07:02:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `kd_supplier` varchar(6) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `kd_supplier`, `nama`, `no_telp`, `email`, `alamat`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'KS0001', 'PT. Supplier 1', '082210734711', 'dedikusnadi207@gmail.com', 'Kp. Caringin Desa Banjarsari Kec. Ciawi Kota Bogor', '2017-08-04 00:39:39', '2017-10-04 02:25:34', NULL),
(2, 'KS0002', 'PT. Supplier 2', '090909090909', 'supplier2@gmail.com', 'Jl Veteran III', '2017-08-04 00:42:11', '2017-10-04 03:12:58', NULL),
(3, 'KS0003', 'update supplierLog', '090909090912', 'updatesupplier@supplier.com', 'Update SupplierLog', '2017-10-02 07:31:37', '2017-10-02 07:32:03', '2017-10-02'),
(4, 'KS0004', 'Dedi', '97987', 'dedikusnadi208@gmail.com', 'Dimana-mana', '2018-01-05 07:02:54', '2018-01-05 07:02:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `no_telp`, `email`, `alamat`, `created_at`, `updated_at`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Dedi Kusnadi', '082210734711', 'dedikusnadi207@gmail.com', 'Kp. Caringin Desa Banjarsari Kec. Ciawi Kab.Bogor Jawa Barat', '2017-07-27 04:56:03', '2017-10-06 03:06:24'),
(4, 'kusnadi', '87509b7f1182014f1948aa8e699848d1', 'Kusnadi', '08221073471100', 'dedikusnadi207@gmail.com', 'SAASD', '2017-08-23 03:26:36', '2017-10-04 04:09:38'),
(5, 'alwi', '86df28556e29bfe0ab2431a6b07c3b01', 'Alwi Aulia Nuryadin', '080808080', 'alwi.nuryadin@gmail.com', 'Kp. Mana-mana', '2017-08-29 04:16:35', '2017-08-29 04:21:25'),
(6, 'updateuserlog', '5d45defc388211d8a51e6c8db2b4d2cf', 'updateuserlog', '091209120912', 'updateuserlog@userlog.com', 'updateuserlog', '2017-10-02 07:32:45', '2017-10-02 07:33:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `no_telp`, `alamat`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'dedi', 'Dedi', '082210734711', 'Kp. Caringin', 'dedikusnadi207@gmail.com', '$2y$10$l.utFhZkOc5Q4PL/am5vhO3k2X2i51ytmqE9IJQkUBoRNtTJ2DUMi', 'CWVUWElnlaI4mlFMuRR68ETD6tepP04ooaUE91Ex5V0y2ARZtnKP9gCKfCXx', '2017-08-07 00:49:39', '2017-08-07 00:49:39');

-- --------------------------------------------------------

--
-- Structure for view `qw_laporan_keluar`
--
DROP TABLE IF EXISTS `qw_laporan_keluar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `qw_laporan_keluar`  AS  select `barang_keluar`.`kd_barang_keluar` AS `kd_barang_keluar`,`barang_keluar`.`tanggal` AS `tanggal`,`barang`.`barcode` AS `barcode`,`barang`.`nama` AS `nama_barang`,`merk`.`nama` AS `nama_merk`,`jenis`.`nama` AS `nama_jenis`,`satuan`.`nama` AS `nama_satuan`,`barang`.`berat` AS `berat`,`detail_barang_keluar`.`jumlah` AS `jumlah` from (((((`barang_keluar` join `detail_barang_keluar` on((`detail_barang_keluar`.`kd_barang_keluar` = `barang_keluar`.`kd_barang_keluar`))) join `barang` on((`barang`.`id` = `detail_barang_keluar`.`barang_id`))) join `merk` on((`merk`.`id` = `barang`.`merk_id`))) join `jenis` on((`jenis`.`id` = `barang`.`jenis_id`))) join `satuan` on((`satuan`.`id` = `barang`.`satuan_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `qw_laporan_masuk`
--
DROP TABLE IF EXISTS `qw_laporan_masuk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `qw_laporan_masuk`  AS  select `barang_masuk`.`kd_barang_masuk` AS `kd_barang_masuk`,`barang_masuk`.`tanggal` AS `tanggal`,`supplier`.`nama` AS `nama_supplier`,`barang`.`barcode` AS `barcode`,`barang`.`nama` AS `nama_barang`,`merk`.`nama` AS `nama_merk`,`jenis`.`nama` AS `nama_jenis`,`satuan`.`nama` AS `nama_satuan`,`barang`.`berat` AS `berat`,`detail_barang_masuk`.`jumlah` AS `jumlah` from ((((((`barang_masuk` join `supplier` on((`supplier`.`id` = `barang_masuk`.`supplier_id`))) join `detail_barang_masuk` on((`detail_barang_masuk`.`kd_barang_masuk` = `barang_masuk`.`kd_barang_masuk`))) join `barang` on((`barang`.`id` = `detail_barang_masuk`.`barang_id`))) join `merk` on((`merk`.`id` = `barang`.`merk_id`))) join `jenis` on((`jenis`.`id` = `barang`.`jenis_id`))) join `satuan` on((`satuan`.`id` = `barang`.`satuan_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kd_barang_UNIQUE` (`barcode`),
  ADD KEY `fk_barang_merk_idx` (`merk_id`),
  ADD KEY `fk_barang_jenis1_idx` (`jenis_id`),
  ADD KEY `fk_barang_satuan1_idx` (`satuan_id`);

--
-- Indexes for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kd_barang_keluar_UNIQUE` (`kd_barang_keluar`);

--
-- Indexes for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kd_barang_masuk_UNIQUE` (`kd_barang_masuk`),
  ADD KEY `fk_barang_masuk_supplier1_idx` (`supplier_id`);

--
-- Indexes for table `detail_barang_keluar`
--
ALTER TABLE `detail_barang_keluar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detail_barang_keluar_barang_keluar1_idx` (`kd_barang_keluar`),
  ADD KEY `fk_detail_barang_keluar_barang1_idx` (`barang_id`);

--
-- Indexes for table `detail_barang_masuk`
--
ALTER TABLE `detail_barang_masuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_detail_barang_masuk_barang_masuk1_idx` (`kd_barang_masuk`),
  ADD KEY `fk_detail_barang_masuk_barang1_idx` (`barang_id`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_aktivitas`
--
ALTER TABLE `log_aktivitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_pengguna`
--
ALTER TABLE `log_pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merk`
--
ALTER TABLE `merk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kd_supplier_UNIQUE` (`kd_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `barang_keluar`
--
ALTER TABLE `barang_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `detail_barang_keluar`
--
ALTER TABLE `detail_barang_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `detail_barang_masuk`
--
ALTER TABLE `detail_barang_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `log_aktivitas`
--
ALTER TABLE `log_aktivitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `log_pengguna`
--
ALTER TABLE `log_pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `merk`
--
ALTER TABLE `merk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barang`
--
ALTER TABLE `barang`
  ADD CONSTRAINT `barang_ibfk_1` FOREIGN KEY (`jenis_id`) REFERENCES `jenis` (`id`),
  ADD CONSTRAINT `barang_ibfk_2` FOREIGN KEY (`merk_id`) REFERENCES `merk` (`id`),
  ADD CONSTRAINT `barang_ibfk_3` FOREIGN KEY (`satuan_id`) REFERENCES `satuan` (`id`);

--
-- Constraints for table `barang_masuk`
--
ALTER TABLE `barang_masuk`
  ADD CONSTRAINT `barang_masuk_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`);

--
-- Constraints for table `detail_barang_keluar`
--
ALTER TABLE `detail_barang_keluar`
  ADD CONSTRAINT `detail_barang_keluar_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`);

--
-- Constraints for table `detail_barang_masuk`
--
ALTER TABLE `detail_barang_masuk`
  ADD CONSTRAINT `detail_barang_masuk_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
