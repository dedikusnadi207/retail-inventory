function fillTable(id,ajax,columns,col = 0,order = 'asc') {
	$(id).dataTable().fnDestroy()
	$(id).DataTable({
		"autoWidth": false,
		processing: false,
		serverSide: false,
		"order": [[ col, order ]],
		ajax: ajax,
		columns: columns
	});
}

function runAjax(type,url,data='') {
	$.ajax({
		type: type,
		url: url,
		data: data,
		error: function(data) {
			console.log(data);
		}
	});
}

function runAjax(type,url,data='',returnData = '') {
	$.ajax({
		type: type,
		url: url,
		data: data,
		success: function(data) {
			if (returnData != '') {
				returnData(data);	
			}
		},
		error: function(data) {
			console.log(data);
			if (returnData != '') {
				returnData(data);	
			}
		}
	});
}
