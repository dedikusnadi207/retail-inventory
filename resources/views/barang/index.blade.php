<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Data Barang</div>
				<div class="panel-body">
           <button type="button" class="btn btn-primary" id="tambah" ><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
           <!-- <button type="button" class="btn btn-success" id="export" ><i class="glyphicon glyphicon-export"></i> Export To Excel</button> -->
           <a href="{{ route('barang.export') }}" class="btn btn-success"><i class="glyphicon glyphicon-export"></i> Export To Excel</a>
				<hr>
					<div class="table-responsive">
						<table class="table table-striped table-bordered" id="data-barang">
							<thead>
								<tr>
									<th>Barcode</th>
									<th>Nama</th>
									<th>Merk</th>
									<th>Jenis</th>
									<th>Satuan</th>
									<th>Berat</th>
									<th>Harga Beli</th>
									<th>Harga Jual</th>
									<th>Stok</th>
									<th>Aksi</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



@include('barang.popup')