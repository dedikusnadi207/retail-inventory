<div class="modal fade" id="yourModal" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Barang</h4>
      </div>
    <div class="modal-body">
      <form class="form-horizontal" id="modalForm">
      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      <div class="form-group">
        <label for="barcode" class="col-md-4 control-label">Barcode</label>
        <div class="col-md-6">
          <input type="text" name="barcode" class="form-control" id="barcode" placeholder="Scan Barcode !">
        </div>
      </div>
      <div class="form-group">
        <label for="nama" class="col-md-4 control-label">Nama Barang</label>
        <div class="col-md-6">
          <input type="text" name="nama" class="form-control" id="nama"  placeholder="Masukkan Nama Barang !" maxlength="45">
        </div>
      </div>
      <div class="form-group">
        <label for="merk_id" class="col-md-4 control-label">Merk Barang</label>
        <div class="col-md-6">
          <select name="merk_id" id="merk_id" class="form-control select2">
            <option value="">-----Pilih Merk-----</option>
            @foreach($listMerk as $key => $val)
              <option value="{{ $key }}">{{ $val }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="jenis_id" class="col-md-4 control-label">Jenis Barang</label>
        <div class="col-md-6">
          <select name="jenis_id" id="jenis_id" class="form-control select2" >
            <option value="">-----Pilih Jenis-----</option>
            @foreach($listJenis as $key => $val)
              <option value="{{ $key }}">{{ $val }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="satuan_id" class="col-md-4 control-label">Satuan Barang</label>
        <div class="col-md-6">
          <select name="satuan_id" id="satuan_id" class="form-control select2" >
            <option value="">-----Pilih Satuan-----</option>
            @foreach($listSatuan as $key => $val)
              <option value="{{ $key }}">{{ $val }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="form-group">
        <label for="berat" class="col-md-4 control-label">Berat Barang</label>
        <div class="col-md-6">
          <input type="number" name="berat" class="form-control" id="berat"  placeholder="Masukkan Berat Barang ! (kg)" maxlength="11">
        </div>
      </div>
      <div class="form-group">
        <label for="harga_beli" class="col-md-4 control-label">Harga Beli</label>
        <div class="col-md-6">
          <input type="number" name="harga_beli" class="form-control" id="harga_beli" min="0" step="100" placeholder="Masukkan Harga Beli Barang !" maxlength="11">
        </div>
      </div>
      <div class="form-group">
        <label for="harga_jual" class="col-md-4 control-label">Harga Jual</label>
        <div class="col-md-6">
          <input type="number" name="harga_jual" class="form-control" id="harga_jual" min="0" step="100"  placeholder="Masukkan Harga Jual Barang !" maxlength="11">
        </div>
      </div>
      <div class="form-group">
        <label for="stok" class="col-md-4 control-label">Stok</label>
        <div class="col-md-6">
          <input type="number" name="stok" class="form-control" id="stok" min="0"  placeholder="Masukkan Stok Barang !" maxlength="11">
        </div>
      </div>  

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="save" value="add">SIMPAN</button>
        <input type="hidden" id="edit_id" name="edit_id" value="0">
      </div>
    </form>
    </div>
    </div>
  </div>
</div>
