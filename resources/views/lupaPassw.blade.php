<section class="content-header">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="panel panel-primary">
      <div class="panel-heading">FORM Lupa Password</div>
        <div class="panel-body">
          <form method="post" id="formLupaPassw">
          {{ csrf_field() }}
            <div class="form-group">
              <label>Username</label>
              <input type="text" class="form-control" placeholder="Username" name="username" id="username">
            </div>
            <div class="form-group" style="display: none;">
              <label>Password Baru</label>
              <input type="password" class="form-control" placeholder="Password Baru" name="newPassw" id="newPassw">
            </div>
            <div class="form-group" style="display: none;">
              <label>Kode Verifikasi</label>
              <input type="text" class="form-control" placeholder="Kode Verifikasi" name="kode" id="kode">
            </div>
            <div class="form-group">       
              <!-- <input type="button" name="kirimKode" id="kirimKode" class="btn btn-danger btn-block" value="KIRIM KODE VIA E-MAIL" onclick="btnKirimKode()"> -->
              <a href="{{ url('/') }}" class="btn btn-default btn-block">KEMBALI</a>
              <a href="#" class="btn btn-danger btn-block" id="btnKirimKode" onclick="btnKirimKode(event)">KIRIM KODE VIA E-MAIL</a>
              <center><label id="lbl" style="display: none;">Harap Tunggu....!</label></center>
              <input type="button" class="btn btn-primary btn-block" id="selesai" style="display: none;" name="submit" placeholder="submit" value="SELESAI" onclick="selesaiForm(this,event)">
            </div>                
          </form>           
        </div>
      </div>
    </div>
  </div>  
</section>
<script type="text/javascript">
  function selesaiForm(data,event) {
    event.preventDefault();
    runAjax("POST"," {{ url('user/lupaPassw') }} ",$("#formLupaPassw").serialize(),function(data) {
      switch(data.status){
        case "username":
          swal("GAGAL !","Username Tidak Terdaftar","error");
        break;
        case "newPassw":
          swal("GAGAL !","Password Tidak Boleh Kosong","error");
        break;
        case "kode":
          swal("GAGAL !","Kode Tidak Sesuai","error");
        break;
        case "success":
          swal({
             title: "Sukses !",
             text: "Data Berhasil Diubah !",
             type: "success",
          },
            function (isConfirm) {
              location.href = "{{ url('/') }}"
            }
          );
        break;

      }      
    });
  }

  function btnKirimKode(event) {
      event.preventDefault();
      $("#btnKirimKode").hide();
      $("#lbl").show();
      runAjax("POST","user/sendPassw",$("#formLupaPassw").serialize(),function(data) {
      switch(data.status){
        case "username":
          swal("GAGAL !","Username Tidak Terdaftar","error");
        break;
        case "success":
          swal({
               title: "Berhasil !",
               text: "Kode Verifikasi Telah Terkirim ke : "+data.email,
               type: "success",
            },
              function () {
                $("#lbl").hide();
                $("#selesai").show();
                $(".form-group").show();
                $("#username").attr("readonly","");
              }
            );
        break;
      }
    });  
  }

  // $("#kirimKode").click(function(e) {
    // e.preventDefault();
  
  // });
</script>