<div class="modal fade" id="yourModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah User</h4>
      </div>
    <div class="modal-body">
      <form class="form-horizontal" id="modalForm">
      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

      <div class="form-group">
        <label for="username" class="col-md-4 control-label">Username</label>
        <div class="col-md-6">
          <input type="text" name="username" class="form-control" id="username"  placeholder="Masukkan Username !" maxlength="100">
        </div>
      </div>
      <div class="form-group" id="form-password">
        <label for="password" class="col-md-4 control-label">Password</label>
        <div class="col-md-6">
          <input type="password" name="password" class="form-control" id="password"  placeholder="Masukkan Password !" maxlength="50">
        </div>
      </div>
      <div class="form-group">
        <label for="nama" class="col-md-4 control-label">Nama</label>
        <div class="col-md-6">
          <input type="text" name="nama" class="form-control" id="nama"  placeholder="Masukkan Nama !" maxlength="50">
        </div>
      </div>
      <div class="form-group">
        <label for="no_telp" class="col-md-4 control-label">Nomor Telepon</label>
        <div class="col-md-6">
          <input type="text" name="no_telp" class="form-control" id="no_telp"  placeholder="Masukkan Nomor Telepon !" maxlength="15">
        </div>
      </div>
      <div class="form-group">
        <label for="email" class="col-md-4 control-label">E-mail</label>
        <div class="col-md-6">
          <input type="email" name="email" class="form-control" id="email"  placeholder="Masukkan E-mail !" maxlength="30">
        </div>
      </div>
      <div class="form-group">
        <label for="alamat" class="col-md-4 control-label">Alamat</label>
        <div class="col-md-6">
        <textarea name="alamat" class="form-control" id="alamat" placeholder="Masukkan Alamat !" style="resize: none;" rows="5"></textarea>
        </div>
      </div>

    </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="save" value="add">SIMPAN</button>
        <input type="hidden" id="edit_id" name="edit_id" value="0">
      </div>
    </div>
    </div>
  </div>
</div>