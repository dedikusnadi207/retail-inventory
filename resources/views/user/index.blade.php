	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading">Data User</div>
					<div class="panel-body">
               <button type="button" class="btn btn-primary" id="tambah" ><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
					<hr>
						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="data-user">
								<thead>
									<tr>
										<th>Username</th>
										<th>Nama</th>
										<th>Nomor Telepon</th>
										<th>E-mail</th>
										<th>Aksi</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@include('user.popup')

