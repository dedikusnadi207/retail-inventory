	<div class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-primary">
					<div class="panel-heading">Data Barang (Terhapus)</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="data-barang">
								<thead>
									<tr>
										<th>Nama Barang</th>
										<th>Aksi</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
