
	<div class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading">Input Barang Keluar</div>
					<div class="panel-body">
						<div class="form-horizontal" id="formSelesai">
      						<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
							
							<div class="form-group">
								<label for="kd_barang_keluar" class="col-md-4 control-label">Kode Barang Keluar</label>
								<div class="col-md-4">
									<input type="text" name="kd_barang_keluar" class="form-control" id="kd_barang_keluar" readonly>
								</div>
							</div>

							<div class="form-group">
								<label for="total_keluar" class="col-md-4 control-label">Total Barang</label>
								<div class="col-md-4">
									<input type="text" name="total_keluar" class="form-control" id="total_keluar" readonly>
								</div>
							</div>

							<div class="form-group" id="end">
								<div class="col-md-8 col-md-offset-4">
									<button type="button" class="btn btn-primary" onclick="selesai()">SELESAI</button>
									<button type="button" class="btn btn-danger" onclick="batal()" id="batal">BATAL</button>
								</div>
							</div>

						</div>
						<hr>
							<div class="form-horizontal" id="formInput">
								<div class="form-group">
									<label for="barcode" class="col-md-4 control-label">Barcode</label>
									<div class="col-md-4">
										<input type="text" name="barcode" class="form-control" id="barcode" placeholder="Scan Barcode !" onkeypress="simpan(this,event)">
									</div>
								</div>
							</div>
						<hr>
						<div class="table-responsive">
							<table class="table table-bordered table-striped" id="detail-keluar">
								<thead>
									<tr>
										<th>Barcode</th>
										<th>Nama</th>
										<th>Merk</th>
										<th>Jenis</th>
										<th>Satuan</th>
										<th>Qty</th>
										<th>+/-</th>
										<th>Aksi</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
