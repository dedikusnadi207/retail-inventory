<div class="modal fade" id="yourModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Jenis</h4>
      </div>
    <div class="modal-body">
      <form class="form-horizontal" id="modalForm" onsubmit="$('#save').click();return false">
      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
      <div class="form-group">
        <label for="nama" class="col-md-4 control-label">Nama Jenis</label>
        <div class="col-md-6">
          <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Nama Jenis Barang !" maxlength="45">
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="save" value="add">SIMPAN</button>
        <input type="hidden" id="edit_id" name="edit_id" value="0">
      </div>
    </form>
    </div>
    </div>
  </div>
</div>