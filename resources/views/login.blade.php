<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISTEM INVENTORY</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('sweetalert/css/sweetalert.css') }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/crud.js') }}"></script>
<script src="{{ asset('sweetalert/js/sweetalert.min.js') }}"></script>

  <script type="text/javascript">
    function lupaPassw(data,event) {
      event.preventDefault();
      $("#isiContent").load($(data).attr('href'));
    }
  </script>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="#" class="navbar-brand"><b>Sistem Inventory</b> | Login</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        

        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container" id="isiContent">
      <!-- Content Header (Page header) -->
      

      <!-- Main content -->
      <section class="content-header">
        <div class="row">
          <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
            <div class="panel-heading">FORM LOGIN</div>
              <div class="panel-body">
                <form method="post" action="{{ url('login') }}">
                {{ csrf_field() }}
                @if((count($errors) > 0) || (session('status')=='salah'))
                  <p class="text-danger">Maaf Username atau Password Anda salah</p>
                @endif
                  <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" placeholder="Username" name="username">
                  </div>
                  <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" placeholder="Password" name="password">
                  </div>
                  <div class="form-group">                  
                    <input type="submit" class="btn btn-primary btn-block" name="submit" placeholder="submit" value="MASUK">
                  </div>                
                  <div class="form-group pull-right">
                    <a href="{{ url('lupaPassw') }}" onclick="lupaPassw(this,event)">Lupa Password ?</a>
                  </div>
                </form>           
              </div>
            </div>
          </div>
        </div>  
        </section>
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <!-- <strong>Copyright &copy; 2017-2018 .</strong> All rights
      reserved. -->
    </div>
    <!-- /.container -->
  </footer>
</div>
</body>
</html>
