<table class="table table-striped table-bordered table-responsive">
	<tr>
		<th width="25%" class="bg-aqua">Pengguna</th>
		<th width="25%" class="bg-aqua">Aktivitas</th>
		<th width="25%" class="bg-aqua">Waktu</th>
		<th width="25%" class="bg-aqua">Deskripsi</th>
	</tr>
	<tr>
		<td width="25%">{{ $datas->user->nama }}</td>
		@if($datas->aktivitas == "U")
		<?php $aktivitas = "Ubah" ?>
		@elseif($datas->aktivitas == "C")
		<?php $aktivitas = "Tambah" ?>
		@else
		<?php $aktivitas = "Hapus" ?>
		@endif
		<td width="25%">{{ $aktivitas }}</td>
		<td width="25%">{{ $datas->created_at }}</td>
		<td width="25%">{{ $datas->aksi }}</td>
	</tr>
</table>
<hr>
<table class="table table-striped table-bordered table-responsive">
	<tr>
		<th colspan="4"  class="bg-aqua">Detail Log</th>
	</tr>
	@if($datas->aktivitas == "U")
	<?php 
		$data_old = json_decode($datas->data_old,TRUE);
		$data_new = json_decode($datas->data_new,TRUE);
		$data_change = json_decode($datas->data_change,TRUE);
	 ?>
	<tr>
		<th colspan="2" width="50%" class="bg-aqua">Data Lama</th>
		<th colspan="2" width="50%" class="bg-aqua">Data Baru</th>
	</tr>
		@foreach($data_old as $key_old => $value_old)
			<tr>
				<td width="25%">{{ $key_old }}</td>
				<td width="25%">{{ $value_old }}</td>
				<td width="25%">{{ $key_old }}</td>
				<td width="25%">{{ $data_new[$key_old] }}</td>
			</tr>
		@endforeach
</table>
<hr>
<table class="table table-striped table-bordered table-responsive">
		
		@if(!empty($data_change))
		<tr>
			<th colspan="4" class="bg-aqua">Perubahan Data</th>
		</tr>
			@foreach($data_change as $key => $value)
				<tr>
					<td colspan="2" width="50%">{{ $key }}</td>
					<td colspan="2" width="50%">{{ $value }}</td>
				</tr>
			@endforeach
		@endif
	@else
		@if($datas->aktivitas == "C")
			<?php $data = json_decode($datas->data_new,TRUE); ?>
		@else
			<?php $data = json_decode($datas->data_old,TRUE); ?>
		@endif

		@foreach($data as $key => $value)
			<tr>
				<td colspan="2" width="50%">{{ $key }}</td>
				<td colspan="2" width="50%">{{ $value }}</td>
			</tr>
		@endforeach
	@endif
</table>