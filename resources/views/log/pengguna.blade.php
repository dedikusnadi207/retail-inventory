<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Log Pengguna</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped" id="log-pengguna">
							<thead>
								<tr>
									<th>Tanggal & Waktu Login</th>
									<th>Tanggal & Waktu Logout</th>
									<th>Username</th>
									<!-- <th>Hapus</th> -->
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>