<div class="modal fade" id="modalDetail" role="dialog" aria-labelledby="myModalLabel" tabindex="-1" style="width: 100%;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Log</h4>
      </div>
      <div class="modal-body" id="modalBody">
      
      </div>
    </div>
  </div>
</div>
