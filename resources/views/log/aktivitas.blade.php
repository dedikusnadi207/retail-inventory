<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Log Aktivitas</div>
				<div class="panel-body">
					<div class="table-responsive" id="cardLayout">
						<table class="table table-bordered table-striped" id="log-aktivitas">
							<thead>
								<tr>
									<th>Menu</th>
									<th>Aktivitas Terakhir</th>
									<th>Aksi</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('log.popUpTambah')