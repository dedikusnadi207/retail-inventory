
<script type="text/javascript">
	refresh();
	function refresh() {
		$.get('keluar/kode',function(data) {
			$("#kd_barang_keluar").attr('value',data.kode);
			$("#total_keluar").attr('value',data.total);
			if (data.total == null) {
				$("#end").hide();
			}else{
				$("#end").show();
			}
			tampil();
		});
	}
	function tampil() {
		var where = $("#kd_barang_keluar").val();
        var columns = [
            { data: 'barcode', name: 'barang.barcode'},
            { data: 'barang', name: 'barang.nama'},
            { data: 'merk', name: 'merk.nama'},
            { data: 'jenis', name: 'jenis.nama'},
            { data: 'satuan', name: 'satuan.nama'},
            { data: 'jumlah', name: 'jumlah'},
            { data: 'plusmin', name: 'plusmin', orderable: false, searchable: false },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ];
		fillTable(
			"#detail-keluar",
			"{!! url('detailKeluar/"+where+"/datatables') !!}",
			columns
			);
	}
	function simpan(data,event) {
		var bar = $(data).val();
		if (event.keyCode == 13 && bar != "") {
			$("#barcode").val("");
			var barang = "";
			var jumlah = 1;
			var type = "POST";
			var url = "detailKeluar";
			$.get('detailKeluar/'+$("#kd_barang_keluar").val()+'/'+bar+'/scan',function(data) {
				barang = data.data;
				detail = data.detail;
				if (barang.stok < jumlah) {
                	swal("Gagal !","Stok habis !", "error");
					return;
				}
				if (data.status == "update") {
					type = "PUT";
					url += '/'+detail.id;
					jumlah = detail.jumlah + 1;
				}
				if (data.status == "null") {
                	swal("Gagal !","Barcode Tidak Terdaftar !", "error");
				}else{
					var field = {
							_token: "{{ csrf_token() }}",
							kd_barang_keluar: $("#kd_barang_keluar").val(),
							barang_id: barang.id,
							jumlah: jumlah
						};
					runAjax(type,url,field);
					refresh();
				}
			});
		}
	}
	function plusmin(data,status) {
		var bar = $(data).val();
		var barang = "";
		var jumlah = 0;
		var cek = $("#qty"+bar).val();
		if (cek == "") {
			return;
		}
		if (status == 1) {
			jumlah = cek;
		}else{
			jumlah = -(cek);
		}
		$("#qty"+bar).val("");
		var type = "PUT";
		var url = "detailKeluar";
		$.get('detailKeluar/'+$("#kd_barang_keluar").val()+'/'+bar+'/scan',function(data) {
			barang = data.data;
			detail = data.detail;
			url += '/'+detail.id;
			if (barang.stok < jumlah) {
            	swal("Gagal !","Stok yang masih tersedia : "+barang.stok+" !", "error");
				return;
			}
			jumlah = parseInt(detail.jumlah) + parseInt(jumlah);
			if (jumlah <= 0) {
                swal("Gagal !","Qty Tidak Boleh Kurang Dari 1 !", "error");
			}else{
				var field =  {
						_token: "{{ csrf_token() }}",
						kd_barang_keluar: $("#kd_barang_keluar").val(),
						barang_id: barang.id,
						jumlah: jumlah
					};
				runAjax(type,url,field);
				refresh();
			}
		});
	}
	function hapus(data) {
		swal({
		   title: "Yakin ingin menghapus data tersebut ?",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",
		   confirmButtonText: "Ya",
		   cancelButtonText: "Tidak",
		   closeOnConfirm: true 
		},
		   function(){
		   	var id = $(data).val();
			runAjax("DELETE","detailKeluar" + '/' + id,{_token: "{{ csrf_token() }}"});
			refresh();
			
		});
	}
	function selesai() {
		var jml = $("#total_keluar").val();
		if (jml == "") {
			swal("Gagal !", "Data Tidak Valid !", "error");
		}else{
			var field = {
				_token: "{{ csrf_token() }}",
				kd_barang_keluar: $("#kd_barang_keluar").val(),
				tanggal: "{{ date('Y-m-d') }}",
				total_keluar: $("#total_keluar").val()
			};
			runAjax("POST","keluar",field);
	        swal("Sukses !", "Data Berhasil disimpan !", "success");
			refresh();
		}
	}
	function batal() {
		swal({
		   title: "Yakin ingin menghapus semua data  ?",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",
		   confirmButtonText: "Ya",
		   cancelButtonText: "Tidak",
		   closeOnConfirm: true 
		},
		   function(){
			var id = $("#kd_barang_keluar").val();
			runAjax("DELETE","detailKeluar/"+id+"/batal",{_token: "{{ csrf_token() }}"});
			refresh();
			
		});
	}
</script>
