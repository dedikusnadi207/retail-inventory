
<script type="text/javascript">
	tampil();
	function tampil() {
		var columns = [
			{data: 'menu', name: 'menu'},
			{data: 'waktu', name: 'waktu'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
		fillTable("#log-aktivitas","{!! url('logAktivitas/datatables') !!}",columns,1,'desc');
	}
	function detailMenu(data) {
		var menu = $(data).val();
		$("#cardLayout").load("{!! url('logAktivitas/table/detailMenu') !!}",function() {
			var columns = [
				{data: 'nama', name: 'nama'},
				{data: 'deskripsi', name: 'deskripsi'},
				{data: 'aktivitas', name: 'aktivitas'},
				{data: 'waktu', name: 'waktu'},
				{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
			];
			fillTable("#log-detailMenu",menu,columns,3,'desc');
				
		});
	}
	function detailLog(data,status) {
		var id = $(data).val();
		$("#modalBody").load("/logAktivitas/table/detailLog/"+id);
		$("#modalDetail").modal("show");
	}
</script>