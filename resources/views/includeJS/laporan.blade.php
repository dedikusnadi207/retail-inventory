<script type="text/javascript">
	$('#periode').daterangepicker({
        locale: {
              format: 'DD MMMM YYYY'
            }
    });
    function exportExcel() {
    	var start = $("#periode").data('daterangepicker').startDate.format('YYYY-MM-DD');
    	var end = $("#periode").data('daterangepicker').endDate.format('YYYY-MM-DD');
        var jenis = $("#jenis").val();
        var link = "";
        if (jenis == "masuk") {
            link = "{{url('laporan/export/masuk/')}}";
        }else{
            link = "{{url('laporan/export/keluar/')}}";
        }
    	$.ajax({
    		url : link,
    		type : "POST",
    		data : {
    				_token: "{{ csrf_token() }}",
					awal: start,
					akhir: end
    		},
    		success:function(msg) {
                if (msg.status == "") {
                    swal("Data Kosong !", "", "error");
                }else{
                    var a = document.createElement("a");
                    a.href = msg.file;
                    a.download = msg.name;
                    document.body.appendChild(a);
                    a.click();
                    a.remove();
                    swal("Data Berhasil Dieksport !", "", "success");
                }
    		},
    		error:function(msg) {
    			console.log(msg)
    		}
    	});
    }
</script>