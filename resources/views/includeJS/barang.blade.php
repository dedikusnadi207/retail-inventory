<script>
    $('.select2').select2();
    $('.select2').css("width","100%");
	function reset() {
		$("#modalForm").trigger('reset');
		$("#merk_id").select2("val",'');
		$("#jenis_id").select2("val",'');
		$("#satuan_id").select2("val",'');
	}
	$("#tambah").click(function(e) {
		$("#myModalLabel").html('Tambah Barang');
		$("#save").val("add").html("SIMPAN");
		$("#yourModal").modal('show');
		reset();
	});
	// $("#export").click(function(e) {
	// 	runAjax("GET","/barang/export",'',function(data) {
			
	// 	});
	// });
	tampil();
	function tampil() {
		var columns = [
	            { data: 'barcode', name: 'barcode' },
	            { data: 'nama', name: 'nama' },
	            { data: 'merk', name: 'merk.nama' },
	            { data: 'jenis', name: 'jenis.nama' },
	            { data: 'satuan', name: 'satuan.nama' },
	            { data: 'berat', name: 'berat' },
	            { data: 'harga_beli', name: 'harga_beli' },
	            { data: 'harga_jual', name: 'harga_jual' },
	            { data: 'stok', name: 'stok' },
	            { data: 'action', name: 'action', orderable: false, searchable: false }
	        ];
	        fillTable("#data-barang","{!! route('barang.datatables') !!}",columns,1);
	        reset();
	}
	function edit(data) {
		var id = $(data).val();
		$("#myModalLabel").html('Edit Barang');
		runAjax("GET",'barang/'+id+'/edit','',function(data) {
			$("#edit_id").val(data.id);

			$("#barcode").val(data.barcode);
			$("#nama").val(data.nama);
			$("#merk_id").select2("val",data.merk_id);
			$("#jenis_id").select2("val",data.jenis_id);
			$("#satuan_id").select2("val",data.satuan_id);
			$("#berat").val(data.berat);
			$("#harga_beli").val(data.harga_beli);
			$("#harga_jual").val(data.harga_jual);
			$("#stok").val(data.stok);

			$("#save").val("update").html("UBAH");
			$("#yourModal").modal('show');
			
		});
	}

	$("#save").click(function () {
        var status = $("#save").val();
        var type = "POST";
        var edit_id = $("#edit_id").val();
        var url = "barang";
        var msg = "Data Berhasil Disimpan !";
        if (status == "update") {
        	type = "PUT";
	        msg = "Data Berhasil Diubah !";
        	url += "/"+ edit_id;
        }
  		runAjax(type,url,$("#modalForm").serialize(),function(data) {
  			console.log(data.status);
  			if (data.status == "success") {
		  		swal("Sukses !", msg, "success");			
		  		if (status == "update") {
					$("#yourModal").modal("hide");
		  		}
		  		tampil();
  			}else if (data.status == "required") {
				swal("Gagal !", "Data Tidak Valid !", "error");
  			}
  		});
	});
	function hapus(data) {
		swal({
		   title: "Yakin ingin menghapus data tersebut ?",
		   text: "Data akan dipindahkan ke dalam Tempat Sampah !",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",
		   confirmButtonText: "Ya",
		   cancelButtonText: "Tidak",
		   closeOnConfirm: false 
		},
		   function(){
		   	var id = $(data).val();
		   	runAjax("DELETE","barang/" + id,{_token: "{{ csrf_token() }}"},function(data) {
		   		if (data.status == "success") {
			   		swal("Sukses !","Data Berhasil Dihapus !", "success"); 
					tampil();
		   		}
		   	});
		});
	}
</script>