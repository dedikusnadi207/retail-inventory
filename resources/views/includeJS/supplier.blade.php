
<script>
	tampil();
	$("#tambah").click(function(e) {
		$(".modal-footer").show();
		$("#myModalLabel").html('Tambah Supplier');
		$("#save").val("add").html("SIMPAN");
		$("#modalForm").trigger('reset');
		$("#yourModal").modal('show');
	});
	function tampil() {
		autoKode();
		var columns = [
	            { data: 'kd_supplier', name: 'kd_supplier'},
	            { data: 'nama', name: 'nama' },
	            { data: 'no_telp', name: 'no_telp' },
	            { data: 'email', name: 'email' },
	            { data: 'action', name: 'action', orderable: false, searchable: false }
	        ];
	        fillTable("#data-supplier","{!! route('supplier.datatables') !!}",columns);
	}
	function autoKode() {
		runAjax("GET",'supplier/kode','',function(data) {
			var kode = "KS";
			var no = parseInt(data.no) + 1;
			if (no < 10) {
				kode += "000" + no;
			}else if (no < 100) {
				kode += "00" + no;
			}else if (no < 1000) {
				kode += "0" + no;
			}else if (no < 10000) {
				kode += no;
			}else{
				kode += "0001";
			}
			$("#kd_supplier").attr('value',kode);
		});
	}
	function set(id) {
		runAjax("GET",'supplier/'+id+'/edit','',function (data) {
			$("#edit_id").val(data.id);
			$("#kd_supplier").val(data.kd_supplier);
			$("#nama").val(data.nama);
			$("#no_telp").val(data.no_telp);
			$("#email").val(data.email);
			$("#alamat").val(data.alamat);
			$("#save").val("update").html("UBAH");
			$("#yourModal").modal('show');
		});
	}
	function edit(data) {
		$("#myModalLabel").html('Edit Supplier');
		$(".modal-footer").show();
		set($(data).val());
	}
	function show(data) {
		$("#myModalLabel").html('Data Supplier');
		$(".modal-footer").hide();
		set($(data).val());
	}

	$("#save").click(function () {
		var status = $("#save").val();
        var type = "POST";
        var edit_id = $("#edit_id").val();
        var url = "supplier";
        var msg = "Data Berhasil Disimpan !";
        if (status == "update") {
        	type = "PUT";
	        msg = "Data Berhasil Diubah !";
        	url += "/"+ edit_id;
        }
  		runAjax(type,url,$("#modalForm").serialize(),function(data) {
  			if (data.status == "success") {
		  		swal("Sukses !", msg, "success");			
				$("#modalForm").trigger('reset');	
		  		if (status == "update") {
					$("#yourModal").modal("hide");
		  		}
		  		tampil();
  			}else if (data.status == "required") {
				swal("Gagal !", "Data Tidak Valid !", "error");
  			}
  		});
	});
	function hapus(data) {
		swal({
		   title: "Yakin ingin menghapus data tersebut ?",
		   text: "Data akan dipindahkan ke dalam Tempat Sampah !",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",
		   confirmButtonText: "Ya",
		   cancelButtonText: "Tidak",
		   closeOnConfirm: false 
		},
		   function(){
		   	var id = $(data).val();
		   	runAjax("DELETE","supplier/" + id,{_token: "{{ csrf_token() }}"},function(data) {
		   		if (data.status == "success") {
			   		swal("Sukses !","Data Berhasil Dihapus !", "success"); 
					tampil();
		   		}
		   	});
		});
	}
</script>