<script type="text/javascript">
	tampil();
	function tampil() {
		var columns = [
			{data: 'nama', name: 'nama'},
			{data: 'action', name: 'action', orderable: false, searchable: false}
		];
		fillTable("#data-supplier","{!! url('recycleBin/supplier/datatables') !!}",columns);
	}
	function hapus(data) {
		swal({
		   title: "Yakin ingin menghapus data tersebut ?",
		   text: "Data tidak akan dapat dikembalikan !",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",
		   confirmButtonText: "Ya",
		   cancelButtonText: "Tidak",
		   closeOnConfirm: true 
		},
		   function(){
		   	var id = $(data).val();
			runAjax("DELETE","/recycleBin/supplier/" + id + "/forceDelete",{_token: "{{ csrf_token() }}"},function(data) {
				if (data.status == "success") {
			        swal("Sukses !", "Data Berhasil dihapus !", "success");
				}else{
			        swal("Gagal !", "Data Telah digunakan !", "error");
				}
			});
			tampil();
		});
	}
	function restore(data) {
		swal({
		   title: "Yakin ingin mengembalikan data tersebut ?",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",
		   confirmButtonText: "Ya",
		   cancelButtonText: "Tidak",
		   closeOnConfirm: true 
		},
		   function(){
		   	var id = $(data).val();
			runAjax("GET","/recycleBin/supplier/" + id + "/restore",{_token: "{{ csrf_token() }}"},function(data) {
		        if (data.status == "success") {
		        	swal("Sukses !", "Data Berhasil dikembalikan !", "success");
		        }
			});
	        tampil();
		});
	}
</script>
