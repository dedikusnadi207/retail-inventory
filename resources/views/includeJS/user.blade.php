
<script>
	tampil();
	$("#tambah").click(function(e) {
		$(".modal-footer").show();	
		$("#myModalLabel").html('Tambah User');
		$("#save").val("add").html("SIMPAN");
		$("#modalForm").trigger('reset');
		$("#form-password").show();
		$("#yourModal").modal('show');

	});
	function tampil() {
		var columns = [
	            { data: 'username', name: 'username'},
	            { data: 'nama', name: 'nama' },
	            { data: 'no_telp', name: 'no_telp' },
	            { data: 'email', name: 'email' },
	            { data: 'action', name: 'action', orderable: false, searchable: false }
	        ];
	        fillTable("#data-user","{!! route('user.datatables') !!}",columns);
	}

	function set(id) {
		runAjax("GET",'user/'+id+'/edit','',function (data) {
			$("#edit_id").val(data.id);
			$("#username").val(data.username);
			$("#nama").val(data.nama);
			$("#no_telp").val(data.no_telp);
			$("#email").val(data.email);
			$("#alamat").val(data.alamat);
			$("#save").val("update").html("UBAH");
			$("#yourModal").modal('show');
		});
	}
	
	function edit(data) {
		$("#myModalLabel").html('Edit User');
		$(".modal-footer").show();
		$("#form-password").hide();
		set($(data).val());
	}
	function show(data) {
		$("#myModalLabel").html('Data User');
		$(".modal-footer").hide();
		$("#form-password").hide();
		set($(data).val());
	}

	$("#save").click(function () {
		var status = $("#save").val();
        var type = "POST";
        var edit_id = $("#edit_id").val();
        var url = "user";
        var msg = "Data Berhasil Disimpan !";
        if (status == "update") {
        	type = "PUT";
	        msg = "Data Berhasil Diubah !";
        	url += "/"+ edit_id;
        }
  		runAjax(type,url,$("#modalForm").serialize(),function(data) {
  			if (data.status == "success") {
		  		swal("Sukses !", msg, "success");			
				$("#modalForm").trigger('reset');	
		  		if (status == "update") {
					$("#yourModal").modal("hide");
		  		}
		  		tampil();
  			}else if (data.status == "required") {
				swal("Gagal !", "Data Tidak Valid !", "error");
  			}
  		});
	});
</script>