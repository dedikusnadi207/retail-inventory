<script>
	tampil();
	$("#tambah").click(function(e) {
		$("#myModalLabel").html('Tambah Jenis');
		$("#save").val("add").html("SIMPAN");
		$("#modalForm").trigger('reset');
		$("#yourModal").modal('show');
	});
	function tampil() {
		var columns = [
	            { data: 'nama', name: 'nama' },
	            { data: 'action', name: 'action', orderable: false, searchable: false }
	        ];
	        fillTable("#data-jenis","{!! route('jenis.datatables') !!}",columns);
	}
	$("#save").click(function () {
		var status = $("#save").val();
        var type = "POST";
        var edit_id = $("#edit_id").val();
        var url = "jenis";
        var msg = "Data Berhasil Disimpan !";
        if (status == "update") {
        	type = "PUT";
	        msg = "Data Berhasil Diubah !";
        	url += "/"+ edit_id;
        }
  		runAjax(type,url,$("#modalForm").serialize(),function(data) {
  			if (data.status == "success") {
		  		swal("Sukses !", msg, "success");			
				$("#modalForm").trigger('reset');	
		  		if (status == "update") {
					$("#yourModal").modal("hide");
		  		}
		  		tampil();
  			}else if (data.status == "required") {
				swal("Gagal !", "Data Tidak Valid !", "error");
  			}
  		});
	});

	function edit(data) {
		var id = $(data).val();
		$("#myModalLabel").html('Edit Jenis');
		runAjax("GET",'jenis/'+id+'/edit','',function(data) {
			$("#edit_id").val(data.id);

			$("#nama").val(data.nama);

			$("#save").val("update").html("UBAH");
			$("#yourModal").modal('show');
		});
	}
	
	function hapus(data) {
		swal({
		   title: "Yakin ingin menghapus data tersebut ?",
		   text: "Data akan dipindahkan ke dalam Tempat Sampah !",
		   type: "warning",
		   showCancelButton: true,
		   confirmButtonColor: "#DD6B55",
		   confirmButtonText: "Ya",
		   cancelButtonText: "Tidak",
		   closeOnConfirm: false 
		},
		   function(){
		   	var id = $(data).val();
		   	runAjax("DELETE","jenis/" + id,{_token: "{{ csrf_token() }}"},function(data) {
		   		if (data.status == "success") {
			   		swal("Sukses !","Data Berhasil Dihapus !", "success"); 
					tampil();
		   		}
		   	});
		});
	}
</script>