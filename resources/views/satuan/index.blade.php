	<div class="content">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-primary">
					<div class="panel-heading">Satuan Barang</div>
					<div class="panel-body">
              		 <button type="button" class="btn btn-primary" id="tambah"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
              		 <hr>
              		 <div class="table-responsive">
						<table class="table table-striped table-bordered" id="data-satuan">
							<thead>
								<tr>
									<th>Nama Satuan</th>
									<th>Aksi</th>
								</tr>
							</thead>
						</table>
              		 </div>
					</div>
				</div>
			</div>
		</div>
	</div>


@include('satuan.popup')
