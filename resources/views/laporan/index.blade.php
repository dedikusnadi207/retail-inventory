<div class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Cetak Laporan Transaksi Barang</div>
				<div class="panel-body">
					<form class="form-horizontal" id="formFilter">
						<div class="form-group">
							<label for="jenis" class="col-md-4 control-label">Jenis Transaksi</label>
							<div class="col-md-4">
				                <div class="input-group" style="width: 100%">
				                	<select class="form-control" id="jenis">
				                		<option value="masuk">Barang Masuk</option>
				                		<option value="keluar">Barang Keluar</option>
				                	</select>
				                </div>
							</div>
						</div>
						<div class="form-group">
							<label for="periode" class="col-md-4 control-label">Periode</label>
							<div class="col-md-4">
				                <div class="input-group">
				                  <div class="input-group-addon">
				                    <i class="fa fa-calendar"></i>
				                  </div>
				                  <input type="text" class="form-control pull-right" id="periode">
				                </div>
							</div>
						</div>
					</form>
					<center>
           				<button class="btn btn-success" id="export" onclick="exportExcel()"><i class="glyphicon glyphicon-export"></i> Export To Excel</button>
					</center>
           			<!-- <a href="#" class="btn btn-success" id="btnTes"><i class="glyphicon glyphicon-export"></i> Export To Excel</a> -->
				</div>
			</div>
		</div>
	</div>

</div>