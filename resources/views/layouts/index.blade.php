<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistem Inventory | Dashboard </title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href=" {{ asset('AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') }} ">
  <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/Ionicons/css/ionicons.min.css') }}">
  <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('AdminLTE/dist/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/morris.js/morris.css') }}">
  <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/jvectormap/jquery-jvectormap.css') }}">
  <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('datatables/css/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('sweetalert/css/sweetalert.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('select2/css/select2.css') }} ">
  <link rel="stylesheet" type="text/css" href="{{ asset('select2/css/select2-bootstrap.css') }} ">


</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="{{url('dashboard')}}" class="logo" onclick="clickMenu(this,event)">
      <span class="logo-mini"><b>S</b>I</span>
      <span class="logo-lg"><b>Sistem</b>Inventory</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <li class="dropdown ">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">{{session('userProfile')->nama}}</span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="#" id="showProfile">Profil</a>
              </li>
              <li>
                <a href="{{ url('logout') }}" id="logout">Keluar</a>
              </li>
            </ul>
          </li>
      </ul>
        
      </div>
    </nav>
  </header>
  
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li id="m-dashboard" class="menu active">
          <a href="{{url('dashboard')}}" onclick="clickMenu(this,event)">
            <i class="fa fa-circle-o"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview" id="master">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Data Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="m-merk" class="menu"><a href="{{ url('merk') }}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Merk Barang</a></li>
            <li id="m-jenis" class="menu"><a href="{{ url('jenis') }}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Jenis Barang</a></li>
            <li id="m-satuan" class="menu"><a href="{{ url('satuan') }}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Satuan Barang</a></li>
            <li id="m-barang" class="menu"><a href="{{ url('barang') }}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Barang</a></li>
            <li id="m-supplier" class="menu"><a href="{{ url('supplier') }}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Supplier</a></li>
            <li id="m-user" class="menu"><a href="{{ url('user') }}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> User</a></li>
          </ul>
        </li>
        <li class="treeview" id="transaksi">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="m-masuk" class="menu"><a href="{{ url('masuk') }}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Barang Masuk</a></li>
            <li id="m-keluar" class="menu"><a href="{{ url('keluar') }}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Barang Keluar</a></li>
          </ul>
        </li>
        <li id="m-laporan" class="menu">
          <a href="{{url('laporan')}}" onclick="clickMenu(this,event)">
            <i class="fa fa-circle-o"></i> <span>Laporan</span>
          </a>
        </li>

        <li class="treeview" id="log">
          <a href="#">
            <i class="fa fa-book"></i> <span>Log</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="m-logAktivitas" class="menu"><a id="logAktivitas" href="{{url('logAktivitas')}}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Aktivitas</a></li>
            <li id="m-logPengguna" class="menu"><a href="{{url('logPengguna')}}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Pengguna</a></li>
          </ul>
        </li>
        <li class="treeview" id="recycle">
          <a href="#">
            <i class="fa fa-trash"></i> <span>Tempat Sampah</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li id="m-Restoremerk" class="menu"><a href="{{url('recycleBin/merk')}}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Merk</a></li>
            <li id="m-Restorejenis" class="menu"><a href="{{url('recycleBin/jenis')}}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Jenis</a></li>
            <li id="m-Restoresatuan" class="menu"><a href="{{url('recycleBin/satuan')}}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Satuan</a></li>
            <li id="m-Restorebarang" class="menu"><a href="{{url('recycleBin/barang')}}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Barang</a></li>
            <li id="m-Restoresupplier" class="menu"><a href="{{url('recycleBin/supplier')}}" onclick="clickMenu(this,event)"><i class="fa fa-circle-o"></i> Supplier</a></li>
          </ul>
        </li>
       
      </ul>
    </section>
  </aside>

  <div class="content-wrapper" id="isiContent">
  </div>
  
  @include('layouts.popUpProfile')
  @include('layouts.popUpPassword')

  <footer class="main-footer">
    <!-- <iframe name="widgetsms" src="http://widget.sms.web.id/" width="270" height="350" frameborder="0"></iframe><br />Cara Membuat Widget <a href="http://sms.web.id" target="_blank">SMS Gratis</a>  click <a href="http://widget.web.id/widget-sms-gratis/" target="_blank">here</a> -->
    <!-- <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 All rights
    reserved. -->
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{ asset('AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('AdminLTE/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('AdminLTE/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('AdminLTE/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('AdminLTE/bower_components/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('AdminLTE/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('AdminLTE/dist/js/demo.js') }}"></script>
<script src="{{ asset('datatables/js/jquery.js') }}"></script>
<script src="{{ asset('sweetalert/js/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('select2/js/select2.full.js') }}"></script>
<script src="{{ asset('js/crud.js') }}"></script>
  <script type="text/javascript">
      

      runAjax("GET","getSession",'',function(data) {
        if (data.session != null) {
          $("#isiContent").load($('#m-'+data.session+' > a').attr('href'));

          var link = "{{ url('ReturnJS') }}";
          $("#js").load(link);

          $(".menu").removeClass('active');
          $('#m-'+data.session).addClass('active');
          $('title').html('Sistem Inventory | '+data.title);

          $('#m-'+data.session).closest("ul").css("display","block").closest("li").addClass('menu-open');
        }
      });
    function clickMenu(data,event) {
      event.preventDefault();
      $("#isiContent").load($(data).attr('href'));

      var link = "{{ url('ReturnJS') }}";
      $("#js").load(link);

      runAjax("GET","getSession",'',function(data) {
        $(".menu").removeClass('active');
        $('#m-'+data.session).addClass('active');
        $('title').html('Sistem Inventory | '+data.title);
      });
    }
    
    $("#showProfile").click(function(e) {
      e.preventDefault();
      $("#modalProfile").modal('show');
      runAjax("GET","getSession",'',function(session) {
        runAjax("GET",'user/'+"{{session('userProfile')->id}}"+'/edit','',function (data) {
          $("#username_profile").val(data.username);
          $("#nama_profile").val(data.nama);
          $("#no_telp_profile").val(data.no_telp);
          $("#email_profile").val(data.email);
          $("#alamat_profile").val(data.alamat);
        });
      });
    });

    $("#simpanPerubahanProfile").click(function(e) {
      runAjax("PUT","user/"+"{{session('userProfile')->id}}",$("#formProfile").serialize(),function(data) {
        if (data.status == "success") {
          $("#modalProfile").modal('hide');
          swal({
             title: "Sukses !",
             text: "Data Berhasil Diubah !",
             type: "success",
          },
            function () {
              location.reload()
            }
          );
        }else if (data.status == "required") {
          swal("Gagal !", "Data Tidak Valid !", "error");
        }
      });
    });

    $("#ubahPassword").click(function(e) {
      $("#modalProfile").modal('hide');
      $("#modalPassword").trigger('reset');
      $("#modalPassword").modal('show');
    });

    $("#simpanPerubahanPassword").click(function(e) {
      e.preventDefault();
      runAjax("POST",'user/changePassw',$("#formPassword").serialize(),function(data) {
        switch(data.status) {
          case "success":
            swal("Sukses !","Password Berhasil Diubah !","success");
            $("#modalPassword").modal('hide');
          break;
          
          case "required":
            swal("Gagal !","Password Tidak Boleh Kosong !","error");
          break;

          case "wrongPassw":
            swal("Gagal !","Password Lama Tidak Sesuai !","error");
          break;

          case "false":
            swal("Gagal !","Konfirmasi Password Tidak Sesuai !","error");
          break;
        }
      });
    });

  </script>

  <div id="js"></div>
</body>
</html>
