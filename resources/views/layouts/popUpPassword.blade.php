<div class="modal fade" id="modalPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Ubah Password</h4>
      </div>
    <div class="modal-body">
      <form class="form-horizontal" id="formPassword">
      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

      <div class="form-group">
        <label for="oldPassw" class="col-md-4 control-label">Password Lama</label>
        <div class="col-md-6">
          <input type="password" name="oldPassw" class="form-control" id="oldPassw" placeholder="Masukkan Password Lama !" maxlength="50">
        </div>
      </div>
      
      <div class="form-group">
        <label for="newPassw" class="col-md-4 control-label">Password Baru</label>
        <div class="col-md-6">
          <input type="password" name="newPassw" class="form-control" id="newPassw" placeholder="Masukkan Password Baru !" maxlength="50">
        </div>
      </div>

      <div class="form-group">
        <label for="confPassw" class="col-md-4 control-label">Konfirmasi Password Baru</label>
        <div class="col-md-6">
          <input type="password" name="confPassw" class="form-control" id="confPassw" placeholder="Masukkan Konfirmasi Password Baru !" maxlength="50">
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary" id="simpanPerubahanPassword" value="add">SIMPAN PERUBAHAN</button>
      </div>
    </form>
    </div>
    </div>
  </div>
</div>