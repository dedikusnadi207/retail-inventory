    <section class="content-header">
      <h1>
      	Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section class="content-header">
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
				<h3>{{ @$datas['stok_barang'] }}</h3>

				<p>Stok Barang Tersedia</p>
				</div>
			<div class="icon">
				<i class="ion ion-edit"></i>
			</div>
			<a href="{{ url('barang') }}" onclick="clickMenu(this,event)" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ @$datas['total_jenis'] }}</h3>

              <p>Total Jenis Barang</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-green">
				<div class="inner">
					<h3>{{ @$datas['total_masuk'] }}</h3>

					<p>Total Barang Masuk</p>
				</div>
			<div class="icon">
				<i class="ion ion-laptop"></i>
			</div>
			<a href="{{ url('masuk') }}" onclick="clickMenu(this,event)" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
					<h3>{{ @$datas['total_keluar'] }}</h3>

					<p>Total Barang Keluar</p>
				</div>
			<div class="icon">
				<i class="ion ion-bag"></i>
			</div>
			<a href="{{ url('keluar') }}" onclick="clickMenu(this,event)" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			</div>
		</div>
    </section>
