<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\barang_masuk;
use App\Model\detail_barang_masuk;
use App\Model\supplier;
use App\Model\log_aktivitas;
use Validator;
use Datatables;
use DB;
class MasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['menu'=>'masuk']);
        $listSupplier = supplier::orderBy('nama','asc')->pluck('nama','id');
        return view('masuk.index',compact('listSupplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = Validator::make($input,barang_masuk::$rules);
        if ($validation->passes()) {
            barang_masuk::create($input);
            // $inputLog = array('username' => session('userProfile')->username, 'aksi' => "Transaksi Barang Masuk '" . $input['kd_barang_masuk'] . "'");
            // log_aktivitas::create($inputLog);
            $response = "success";
        }else{
            $response = "fail";
        }
        return response()->json(['status'=>$response]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function datatables()
    {
        # code...
    }

    public function kode()
    {
        $kode = "KBM".date('Ymd');
        $barang_masuk = DB::select(DB::raw('SELECT RIGHT(kd_barang_masuk,3) as no FROM barang_masuk WHERE LEFT(kd_barang_masuk,11)=:kd ORDER BY no DESC LIMIT 1'),array('kd' => $kode ));
        if ($barang_masuk != null) {
            $barang_masuk = $barang_masuk[0];
            $no = $barang_masuk->no + 1;
            // $no = 1;
        }else{
            $no = 1;
        }

        if ($no < 10) {
            $kode .= "00" . $no;
        }elseif ($no < 100) {
            $kode .= "0" . $no;
        }elseif ($no < 1000) {
            $kode .=  $no;
        }else{
            $kode .= "0001";
        }

        $total = detail_barang_masuk::select(DB::raw('SUM(jumlah) as jumlah'))->where('kd_barang_masuk','=',$kode)->first();
        $total = $total->jumlah;
        return response()->json(['kode'=>$kode, 'total'=>$total]);
    }
}
