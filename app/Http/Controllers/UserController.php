<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\user;
// use App\Model\log_pengguna;
use App\Http\Controllers\LogAktivitasController;
use Validator;
use Datatables;
use Mail;

use App\Model\barang;
use App\Model\barang_masuk;
use App\Model\barang_keluar;
use App\Model\jenis;
use DB;
class UserController extends Controller
{
	public function index()
    {
        session(['menu'=>'user']);
        return view('user.index');
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['password'] = md5($request->password);
        $validation = Validator::make($input,user::$rules);
        if ($validation->passes()) {
            
            $dataNew = array(
                'Username' => $request->username,
                'Nama' => $request->nama,
                'Nomor Telepon' => $request->no_telp,
                'Email' => $request->email,
                'Alamat' => $request->alamat
            );
            $log = array($dataNew);
            LogAktivitasController::simpan($log,"Berhasil Menambahkan User ".$request->nama,"User",1);
            
            user::create($input);
            $response = array('status' => 'success');
        }else{
            $response = array('status' => 'required');
        }
        return response()->json($response);
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
        $data = user::findOrFail($id);
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $input = [
            'username'=>$request->username,
            'nama'=>$request->nama,
            'no_telp'=>$request->no_telp,
            'email'=>$request->email,
            'alamat'=>$request->alamat
            ];
        $user = user::findOrFail($id);
        $validation = Validator::make($input,[
            'username' => 'required',
            'nama'   => 'required',
            'no_telp' => 'required',
            'email' => 'required|email',
            'alamat'  => 'required'
            ]);
        if ($validation->passes()) {
            
            $dataNew = array(
                'Username' => $request->username,
                'Nama' => $request->nama,
                'Nomor Telepon' => $request->no_telp,
                'Email' => $request->email,
                'Alamat' => $request->alamat
            );
            $dataOld = array(
                'Username' => $user->username,
                'Nama' => $user->nama,
                'Nomor Telepon' => $user->no_telp,
                'Email' => $user->email,
                'Alamat' => $user->alamat
            );
            $dataChange = array_diff_assoc($dataNew, $dataOld);
            $log = array($dataNew,$dataOld,$dataChange);
            LogAktivitasController::simpan($log,"Berhasil Mengubah User ".$request->nama,"User",2);
            
            $user->update($input);
            $response = array('status' => 'success' );
        }else{
            $response = array('status' => 'required' );
        }
        $cek = user::where('id','=',session('userProfile')->id)->first();
        session(['userProfile'=>$cek]);
        return response()->json($response);
    }

    public function destroy($id)
    {
    }

    public function datatables()
    {
        $user = user::where('id','<>',session('userProfile')->id)->get();
        return Datatables::of($user)
                ->addColumn('action',function($data)
                {
                    return  
                    "<form class='form-horizontal' id='formDelete'>".
                    "<input type='hidden' name='_token' value='".csrf_token()."'>".
                    "<button type='button' class='btn btn-primary btn-sm' value='".$data->id."' onclick='show(this)'><i class='glyphicon glyphicon-eye-open'></i></button>".
                    "<button type='button' class='btn btn-default btn-sm' value='".$data->id."' onclick='edit(this)'><i class='glyphicon glyphicon-edit'></i></button>".
                    "</form>";
                })->make(true);
    }

	function login(Request $req)
	{
		$valid = Validator::make($req->all(),[
				'username' => 'required|min:3',
				'password' => 'required|min:3'
			]);
		if ($valid->fails()) {
			return redirect('/')->withErrors($valid)->withInput();
		}

		$user = $req->username;
		$pass = md5($req->password);
		$cek = user::where(['username'=>$user,'password'=>$pass]);
		if ($cek->count() > 0) {
			$data = $cek->first();
            session(['userProfile'=>$data]);
            session(['menu'=>'dashboard']);
            // $logPengguna = array('username' => session('userProfile')->username, 'status' => 'in');
            // log_pengguna::create($logPengguna);
			return redirect('/');

		}else{
			return redirect('/')->with('status','salah');
		}
	}
	function logout()
	{
        $data = array('status' => 'out');
        // $logPengguna = log_pengguna::where(['username' => session('userProfile')->username, 'status' => 'in']);
        // $logPengguna->update($data);
		session()->regenerate();
		session()->flush();
		return redirect('/');
	}

    function changePassword(Request $request)
    {
        $oldPassw = $request->oldPassw;
        $newPassw = $request->newPassw;
        $confPassw = $request->confPassw;
        if (md5($oldPassw) != session('userProfile')->password) {
            return response()->json(['status'=>'wrongPassw']);
        }elseif (empty($newPassw)) {
            return response()->json(['status'=>'required']);
        }elseif ($newPassw != $confPassw) {
            return response()->json(['status'=>'false']);
        }
        $user = user::findOrFail(session('userProfile')->id);
        $user->update(['password'=>md5($newPassw)]);
        $cek = user::where('id','=',session('userProfile')->id)->first();
        session(['userProfile'=>$cek]);
        return response()->json(['status'=>'success']);
    }

    function lupaPassw(Request $request)
    {
        $username = $request->username;
        $newPassw = $request->newPassw;
        $inputKode = $request->kode;
        $user = user::where(['username'=>$username]);
        if ($user->count() <= 0) {
            return response()->json(['status'=>'username']);
        }else if (empty($newPassw)) {
            return response()->json(['status'=>'newPassw']);
        }
        if ($inputKode != session('kode')) {
            return response()->json(['status'=>'kode']);
        }

        $user->update(['password'=>md5($newPassw)]);
        session()->forget('kode');
        session(['userProfile' => $user->first()]);
        session(['menu'=>'dashboard']);
        return response()->json(['status'=>'success']);
    }

    function kirimKode(Request $request)
    {
        $username = $request->username;
        $user = user::where(['username'=>$username]);
        if ($user->count() <= 0) {
            return response()->json(['status'=>'username']);
        }
        $user = $user->first();
        $kode = rand();
        session(['kode'=>$kode]);

        Mail::send('lupaPassw.index',['kode' => $kode], function ($message) use ($user)
        {
            $message->to($user->email)->from('alradipkl@gmail.com','SISTEM INVENTORY')->subject('Kode Verifikasi User');
        });
        return response()->json(['status'=>'success','email'=>$user->email]);
    }

    public function dashboard()
    {
        session(['menu'=>'dashboard']);
        $barang = barang::select(DB::raw('SUM(stok) as stok'))->first();
        $total_masuk = barang_masuk::select(DB::raw('SUM(total_masuk) as total_masuk'))->first();
        $total_keluar = barang_keluar::select(DB::raw('SUM(total_keluar) as total_keluar'))->first();
        $total_keluar = barang_keluar::select(DB::raw('SUM(total_keluar) as total_keluar'))->first();
        $total_jenis = jenis::get()->count();
        $datas = array(
            'stok_barang' => $barang->stok,
            'total_masuk' => $total_masuk->total_masuk,
            'total_keluar' => $total_keluar->total_keluar,
            'total_jenis' => $total_jenis
            );
        // return redirect('/')->with(compact('datas'));

        return view('dashboard',compact('datas'));
    }
}
