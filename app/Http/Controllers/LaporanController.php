<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
class LaporanController extends Controller
{
	public function index()
	{
		session(['menu'=>'laporan']);
		return view('laporan.index');
	}

	public function masuk(Request $req)
	{
		$awal = $req->awal;
		$akhir = $req->akhir;
		$datas = DB::table('qw_laporan_masuk')->whereBetween('tanggal',[$awal,$akhir])->orderBy('kd_barang_masuk','asc')->get();
		$no = 1;
		$formatAwal = date_format(date_create($awal),'d M Y');
		$formatAkhir = date_format(date_create($akhir),'d M Y');
        if ($datas->isEmpty()) {
		  return response()->json(['status'=>'']);
        }
		// dd($req->awal);
		$title ='Laporan Barang Masuk ('.$formatAwal.' - '.$formatAkhir.')';
		$excel = Excel::create($title,function($excel) use ($datas,$no,$formatAwal,$formatAkhir)
		{
			$excel->sheet('Barang Masuk',function($sheet) use ($datas,$no,$formatAwal,$formatAkhir)
			{
				$sheet->mergeCells('A1:K1');
				$sheet->cell('A1',function($cell)
				{
					$cell->setValue('Laporan Transaksi Barang Masuk');
				});
				
				$sheet->mergeCells('A2:K2');
				$sheet->cell('A2',function($cell)
				{
					$cell->setValue('Periode');
				});

				$sheet->mergeCells('A3:K3');
				$sheet->cell('A3',function($cell) use ($formatAwal,$formatAkhir)
				{
					$cell->setValue($formatAwal.' - '.$formatAkhir);
				});

                $sheet->cells("A1:A3",function($cells)
                {
                    $cells->setAlignment('center');
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '16',
                        'bold'       =>  true
                    ));
                });

                $sheet->cells('A5:K5',function ($cells)
                {
                	$cells->setBackground("#3366ff");
                    $cells->setAlignment('center');
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
				$columns = array('No','Kode Barang Masuk','Tanggal','Supplier','Barcode','Nama Barang','Merk','Jenis','Satuan','Berat','Jumlah');
                $sheet->row(5,$columns);

                $row = 6;
                $totalJumlah = 0;
                $merge = true;
                foreach ($datas as $data) {
                	$rowCount = DB::table('qw_laporan_masuk')->where('kd_barang_masuk','=',$data->kd_barang_masuk)->count();
                	$arrayName = array($no);
                	array_push($arrayName, 
                		$data->kd_barang_masuk,
                		date_format(date_create($data->tanggal),'d M Y'),
                		$data->nama_supplier,
                		$data->barcode,
                		$data->nama_barang,
                		$data->nama_merk,
                		$data->nama_jenis,
                		$data->nama_satuan,
                		$data->berat." kg",
                		$data->jumlah
                		);
                	if ($rowCount > 1) {
                		if ($merge) {
                			$no++;
	                		$sheet->mergeCells('A'.$row.':A'.($row+$rowCount-1));
	                		$sheet->cell('A'.$row,function($cell) use ($no)
							{
								$cell->setValue($no);
							});
	                		$sheet->mergeCells('B'.$row.':B'.($row+$rowCount-1));
	                		$sheet->cell('B'.$row,function($cell) use ($data)
							{
								$cell->setValue($data->kd_barang_masuk);
							});
	                		$sheet->mergeCells('C'.$row.':C'.($row+$rowCount-1));
	                		$sheet->cell('C'.$row,function($cell) use ($data)
							{
								$cell->setValue($data->tanggal);
							});
	                		$sheet->mergeCells('D'.$row.':D'.($row+$rowCount-1));
	                		$sheet->cell('D'.$row,function($cell) use ($data)
							{
								$cell->setValue($data->nama_supplier);
							});
							$merge = false;
                		}
                	}else{
                		$merge = true;
                        $no++;
                    }
                		$sheet->row($row,$arrayName);
                	$row++;
                	$totalJumlah +=$data->jumlah;
                }
                $no--;
                $row--;
                $sheet->setBorder('A5:K'.$row,'thin');
                $sheet->cells('A6:D'.$row,function($cells)
                {
                    $cells->setAlignment('center');
                    $cells->setValignment('top');
                });
                $sheet->cells('E6:J'.$row,function($cells)
                {
                	$cells->setAlignment('left');
                });
                $sheet->mergeCells("I".($row+1).":J".($row+1));
                $sheet->cells("I".($row+1).":J".($row+1),function($cells)
                {
                    $cells->setAlignment('right');
                    $cells->setFontWeight('bold');
                    $cells->setBorder('thin','thin','thin','thin');
                });
                $sheet->cell("I".($row+1),function($cells)
                {
                    $cells->setValue("Total");
                });
                $sheet->cells('K6:K'.($row+1),function($cells)
                {
                	$cells->setAlignment('right');
                });
                $sheet->cell("K".($row+1),function($cells) use ($totalJumlah)
                {
                    $cells->setValue($totalJumlah);
                    $cells->setFontWeight('bold');
                    $cells->setBorder('thin','thin','thin','thin');
                });
			});
		});
        $myFile = $excel->string('xlsx'); //change xlsx for the format you want, default is xls
        $response =  array(
           'name' => $title, //no extention needed
           'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
        );
        return response()->json($response);
	}

    public function keluar(Request $req)
    {
        $awal = $req->awal;
        $akhir = $req->akhir;
        $datas = DB::table('qw_laporan_keluar')->whereBetween('tanggal',[$awal,$akhir])->orderBy('kd_barang_keluar','asc')->get();
        $no = 1;
        $formatAwal = date_format(date_create($awal),'d M Y');
        $formatAkhir = date_format(date_create($akhir),'d M Y');
        if ($datas->isEmpty()) {
          return response()->json(['status'=>'']);
        }

        $title ='Laporan Barang Keluar ('.$formatAwal.' - '.$formatAkhir.')';
        $excel = Excel::create($title,function($excel) use ($datas,$no,$formatAwal,$formatAkhir)
        {
            $excel->sheet('Barang Keluar',function($sheet) use ($datas,$no,$formatAwal,$formatAkhir)
            {
                $sheet->mergeCells('A1:J1');
                $sheet->cell('A1',function($cell)
                {
                    $cell->setValue('Laporan Transaksi Barang Keluar');
                });
                
                $sheet->mergeCells('A2:J2');
                $sheet->cell('A2',function($cell)
                {
                    $cell->setValue('Periode');
                });

                $sheet->mergeCells('A3:J3');
                $sheet->cell('A3',function($cell) use ($formatAwal,$formatAkhir)
                {
                    $cell->setValue($formatAwal.' - '.$formatAkhir);
                });

                $sheet->cells("A1:A3",function($cells)
                {
                    $cells->setAlignment('center');
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '16',
                        'bold'       =>  true
                    ));
                });

                $sheet->cells('A5:J5',function ($cells)
                {
                    $cells->setBackground("#3366ff");
                    $cells->setAlignment('center');
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $columns = array('No','Kode Barang Keluar','Tanggal','Barcode','Nama Barang','Merk','Jenis','Satuan','Berat','Jumlah');
                $sheet->row(5,$columns);

                $row = 6;
                $totalJumlah = 0;
                $merge = true;
                foreach ($datas as $data) {
                    $rowCount = DB::table('qw_laporan_keluar')->where('kd_barang_keluar','=',$data->kd_barang_keluar)->count();
                    $arrayName = array($no);
                    array_push($arrayName, 
                        $data->kd_barang_keluar,
                        date_format(date_create($data->tanggal),'d M Y'),
                        $data->barcode,
                        $data->nama_barang,
                        $data->nama_merk,
                        $data->nama_jenis,
                        $data->nama_satuan,
                        $data->berat." kg",
                        $data->jumlah
                        );
                    if ($rowCount > 1) {
                        if ($merge) {
                            $no++;
                            $sheet->mergeCells('A'.$row.':A'.($row+$rowCount-1));
                            $sheet->cell('A'.$row,function($cell) use ($no)
                            {
                                $cell->setValue($no);
                            });
                            $sheet->mergeCells('B'.$row.':B'.($row+$rowCount-1));
                            $sheet->cell('B'.$row,function($cell) use ($data)
                            {
                                $cell->setValue($data->kd_barang_keluar);
                            });
                            $sheet->mergeCells('C'.$row.':C'.($row+$rowCount-1));
                            $sheet->cell('C'.$row,function($cell) use ($data)
                            {
                                $cell->setValue($data->tanggal);
                            });
                            $merge = false;
                        }
                    }else{
                        $merge = true;
                        $no++;
                    }
                        $sheet->row($row,$arrayName);
                    $row++;
                    $totalJumlah +=$data->jumlah;
                }
                $no--;
                $row--;
                $sheet->setBorder('A5:J'.$row,'thin');
                $sheet->cells('A6:C'.$row,function($cells)
                {
                    $cells->setAlignment('center');
                    $cells->setValignment('top');
                });
                $sheet->cells('D6:I'.$row,function($cells)
                {
                    $cells->setAlignment('left');
                });
                $sheet->mergeCells("H".($row+1).":I".($row+1));
                $sheet->cells("H".($row+1).":I".($row+1),function($cells)
                {
                    $cells->setAlignment('right');
                    $cells->setFontWeight('bold');
                    $cells->setBorder('thin','thin','thin','thin');
                });
                $sheet->cell("H".($row+1),function($cells)
                {
                    $cells->setValue("Total");
                });
                $sheet->cells('J6:J'.($row+1),function($cells)
                {
                    $cells->setAlignment('right');
                });
                $sheet->cell("J".($row+1),function($cells) use ($totalJumlah)
                {
                    $cells->setValue($totalJumlah);
                    $cells->setFontWeight('bold');
                    $cells->setBorder('thin','thin','thin','thin');
                });
            });
        });
        $myFile = $excel->string('xlsx'); //change xlsx for the format you want, default is xls
        $response =  array(
           'name' => $title, //no extention needed
           'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($myFile) //mime type of used format
        );
        return response()->json($response);
    }

}