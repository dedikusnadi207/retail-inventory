<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\jenis;
use App\Http\Controllers\LogAktivitasController;
use Datatables;
use Validator;
class JenisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['menu'=>'jenis']);
        // $datas = jenis::paginate(5);
        // return view('jenis.index')->with(compact('datas'));
        return view('jenis.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = Validator::make($input,jenis::$rules);
        if ($validation->passes()) {

            $dataNew = array(
                "Nama" => $request->nama
            );
            $log = array($dataNew);
            LogAktivitasController::simpan($log,"Berhasil Menambahkan Jenis ".$request->nama,"Jenis",1);

            jenis::create($input);
            $response = array('status' => 'success');
        }else{
            $response = array('status' => 'required');
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = jenis::findOrFail($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $jenis = jenis::findOrFail($id);
        $validation = Validator::make($input,jenis::$rules);
        if ($validation->passes()) {

            $dataNew = array(
                "Nama" => $request->nama
            );
            $dataOld = array(
                "Nama" => $jenis->nama
            );
            $dataChange = array_diff_assoc($dataNew, $dataOld);
            $log = array($dataNew,$dataOld,$dataChange);
            LogAktivitasController::simpan($log,"Berhasil Mengubah Jenis ".$request->nama,"Jenis",2);

            $jenis->update($input);
            $response = array('status' => 'success' );
        }else{
            $response = array('status' => 'required' );
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jenis = jenis::findOrFail($id);

        $dataOld = array(
            "Nama" => $jenis->nama
        );
        $log = array($dataOld);
        LogAktivitasController::simpan($log,"Berhasil Menghapus Jenis ".$jenis->nama,"Jenis",3);
        
        $jenis->delete();
        $response = array('status' => 'success' );
        return response()->json($response);
    }
    public function datatables()
    {
        $jenis = jenis::query();
        return Datatables::of($jenis)
                ->addColumn('action',function($data)
                {
                    return  
                    "<form class='form-horizontal' id='formDelete'>".
                    "<input type='hidden' name='_token' value='".csrf_token()."'>".
                    "<button type='button' class='btn btn-default btn-sm' value='".$data->id."' onclick='edit(this)'><i class='glyphicon glyphicon-edit'></i></button>".
                    "<button type='button' class='btn btn-danger btn-sm'  value='".$data->id."' onclick='hapus(this)'><i class='glyphicon glyphicon-trash'></i></button>".
                    "</form>";
                })->make(true);
    }
}
