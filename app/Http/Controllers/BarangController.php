<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\barang;
use App\Model\jenis;
use App\Model\merk;
use App\Model\satuan;
use App\Model\log_aktivitas;
use App\Http\Controllers\LogAktivitasController;
use Validator;
use Datatables;
use DB;
use Excel;
class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        session(['menu'=>'barang']);
        $listMerk = merk::orderBy('nama','asc')->pluck('nama','id');
        $listJenis = jenis::orderBy('nama','asc')->pluck('nama','id');
        $listSatuan = satuan::orderBy('nama','asc')->pluck('nama','id');
        return view('barang.index',compact('listMerk','listSatuan','listJenis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = Validator::make($input,barang::$rules);
        if ($validation->passes()) {

            $dataNew = array(
                'Barcode' => $request->barcode,
                'Nama' => $request->nama,
                'Merk' => merk::findOrFail($request->merk_id)->nama,
                'Jenis' => jenis::findOrFail($request->jenis_id)->nama,
                'Satuan' => satuan::findOrFail($request->satuan_id)->nama,
                'Berat' => $request->berat ,
                'Harga Beli' => $request->harga_beli,
                'Harga Jual' => $request->harga_jual,
                'Stok' => $request->stok
            );
            $log = array($dataNew);
            LogAktivitasController::simpan($log,"Berhasil Menambahkan Barang ".$request->nama,"Barang",1);

            barang::create($input);
            $response = array('status' => 'success');
        }else{
            $response = array('status' => 'required');
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = barang::findOrFail($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $barang = barang::findOrFail($id);
        $validation = Validator::make($input,barang::$rules);
        if ($validation->passes()) {

            $dataNew = array(
                'Barcode' => $request->barcode,
                'Nama' => $request->nama,
                'Merk' => merk::findOrFail($request->merk_id)->nama,
                'Jenis' => jenis::findOrFail($request->jenis_id)->nama,
                'Satuan' => satuan::findOrFail($request->satuan_id)->nama,
                'Berat' => $request->berat ,
                'Harga Beli' => $request->harga_beli,
                'Harga Jual' => $request->harga_jual,
                'Stok' => $request->stok
            );
            $dataOld = array(
                'Barcode' => $barang->barcode,
                'Nama' => $barang->nama,
                'Merk' => merk::findOrFail($barang->merk_id)->nama,
                'Jenis' => jenis::findOrFail($barang->jenis_id)->nama,
                'Satuan' => satuan::findOrFail($barang->satuan_id)->nama,
                'Berat' => $barang->berat ,
                'Harga Beli' => $barang->harga_beli,
                'Harga Jual' => $barang->harga_jual,
                'Stok' => $barang->stok
            );
            $dataChange = array_diff_assoc($dataNew, $dataOld);
            $log = array($dataNew,$dataOld,$dataChange);
            LogAktivitasController::simpan($log,"Berhasil Mengubah Barang ".$request->nama,"Barang",2);

            $barang->update($input);
            // $inputLog = array('username' => session('userProfile')->username, 'aksi' => "Mengubah Barang '" . $input['nama'] . "'");
            // log_aktivitas::create($inputLog);
            $response = array('status' => 'success' );
        }else{
            $response = array('status' => 'required' );
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = barang::findOrFail($id);

        $dataOld = array(
                'Barcode' => $barang->barcode,
                'Nama' => $barang->nama,
                'Merk' => merk::findOrFail($barang->merk_id)->nama,
                'Jenis' => jenis::findOrFail($barang->jenis_id)->nama,
                'Satuan' => satuan::findOrFail($barang->satuan_id)->nama,
                'Berat' => $barang->berat ,
                'Harga Beli' => $barang->harga_beli,
                'Harga Jual' => $barang->harga_jual,
                'Stok' => $barang->stok
            );
        $log = array($dataOld);
        LogAktivitasController::simpan($log,"Berhasil Menghapus Barang ".$barang->nama,"Barang",3);
        
        $barang->delete();
        $response = array('status' => 'success' );
        return response()->json($response);
    }

    public function datatables()
    {
        $barang = barang::join('merk','merk.id','=','barang.merk_id')
                        ->join('jenis','jenis.id','=','barang.jenis_id')
                        ->join('satuan','satuan.id','=','barang.satuan_id')
                        ->select(
                            [
                                'barang.id as id',
                                'barcode',
                                'barang.nama as nama',
                                'merk.nama as merk',
                                'jenis.nama as jenis',
                                'satuan.nama as satuan',
                                'berat',
                                'harga_beli',
                                'harga_jual',
                                'stok'
                            ]);
        return Datatables::of($barang)
                ->addColumn('action',function($data)
                {
                    return  
                    "<form class='form-horizontal' id='formDelete'>".
                    "<input type='hidden' name='_token' value='".csrf_token()."'>".
                    "<button type='button' class='btn btn-default btn-sm' value='".$data->id."' onclick='edit(this)'><i class='glyphicon glyphicon-edit'></i></button>".
                    "<button type='button' class='btn btn-danger btn-sm'  value='".$data->id."' onclick='hapus(this)'><i class='glyphicon glyphicon-trash'></i></button>".
                    "</form>";
                })->make(true);
    }

    public function export()
    {
        $datas = barang::join('merk','merk.id','=','barang.merk_id')
                        ->join('jenis','jenis.id','=','barang.jenis_id')
                        ->join('satuan','satuan.id','=','barang.satuan_id')
                        ->select(
                            [
                                'barcode',
                                'barang.nama as nama',
                                'merk.nama as merk',
                                'jenis.nama as jenis',
                                'satuan.nama as satuan',
                                'berat',
                                'harga_beli',
                                'harga_jual',
                                'stok'
                            ])->orderBy('nama','asc')->get();
        $no = 1;
        Excel::create('Laporan Stok Barang '. date('j F Y'),function($excel) use ($datas,$no)
        {
            $excel->sheet('Data Barang',function($sheet) use ($datas,$no)
            {
                $sheet->mergeCells("A1:J1");
                $sheet->cell('A1', function($cell) {
                    $cell->setValue('Laporan Stok Barang');
                });

                $sheet->mergeCells("A2:J2");
                $sheet->cell('A2', function($cell) {
                    $cell->setValue('Tanggal '.date('j F Y'));
                });

                $sheet->cells("A1:A2",function($cells)
                {
                    $cells->setAlignment('center');
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '16',
                        'bold'       =>  true
                    ));
                });

                $sheet->cells("A4:J4",function($cells)
                {
                    $cells->setBackground("#3366ff");
                    $cells->setAlignment('center');
                    $cells->setFont(array(
                        'family'     => 'Calibri',
                        'size'       => '12',
                        'bold'       =>  true
                    ));
                });
                $columns = array('No','Barcode','Nama Barang','Merk','Jenis','Satuan','Berat','Harga Beli','Harga Jual','Stok');
                $sheet->row(4,$columns);

                $row = 5;
                $allStok = 0;
                foreach ($datas as $data) {
                    $arrayName = array($no);
                    array_push($arrayName,
                     $data->barcode,
                     $data->nama,
                     $data->merk,
                     $data->jenis,
                     $data->satuan,
                     $data->berat,
                     $data->harga_beli,
                     $data->harga_jual,
                     $data->stok
                     );
                    $sheet->row($row,$arrayName);
                    $no++;
                    $row++;
                    $allStok += $data->stok;
                }
                $no-=1;
                $row-=1;
                $sheet->setBorder('A4:J'.$row, 'thin');
                $sheet->cells("A5:A".$row,function($cells)
                {
                    $cells->setAlignment('center');
                });
                $sheet->cells("B5:F".$row,function($cells)
                {
                    $cells->setAlignment('left');
                });
                $sheet->cells("G5:J".$row,function($cells)
                {
                    $cells->setAlignment('right');
                });
                $sheet->mergeCells("H".($row+1).":I".($row+1));
                $sheet->cells("H".($row+1).":I".($row+1),function($cells)
                {
                    $cells->setAlignment('right');
                    $cells->setFontWeight('bold');
                    $cells->setBorder('thin','thin','thin','thin');
                });
                $sheet->cell("H".($row+1),function($cells)
                {
                    $cells->setValue("Total Stok");
                });
                $sheet->cell("J".($row+1),function($cells) use ($allStok)
                {
                    $cells->setValue($allStok);
                    $cells->setFontWeight('bold');
                    $cells->setBorder('thin','thin','thin','thin');
                });

            });
        })->download('xlsx');
    }

}
