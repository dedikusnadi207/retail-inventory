<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\detail_barang_keluar;
use App\Model\barang;
use Validator;
use Datatables;
use DB;

class DetailKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        detail_barang_keluar::create($input);
        return response()->json(['status'=>"success"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $detail = detail_barang_keluar::findOrFail($id);
        $detail->update($input);
        return response()->json(['status'=>"success"]);
        // return response()->json(['tes'=>'Ini dari update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail = detail_barang_keluar::findOrFail($id);
        $detail->delete();
        return response()->json(['status'=>"success"]);
    }

    public function datatables($where)
    {
        $detail = detail_barang_keluar::join('barang','barang.id','=','detail_barang_keluar.barang_id')
        ->join('merk','merk.id','=','barang.merk_id')
        ->join('jenis','jenis.id','=','barang.jenis_id')
        ->join('satuan','satuan.id','=','barang.satuan_id')
        ->select([
                'detail_barang_keluar.id as id',
                'detail_barang_keluar.kd_barang_keluar',
                'barang.barcode',
                'barang.nama as barang',
                'merk.nama as merk',
                'jenis.nama as jenis',
                'satuan.nama as satuan',
                'detail_barang_keluar.jumlah'
            ])->where('detail_barang_keluar.kd_barang_keluar','=',$where);
        return Datatables::of($detail)
                ->addColumn('plusmin',function($data)
                {
                    return 
                    "<form class='form-horizontal' id=''>".
                    "<button type='button' class='btn btn-primary btn-sm' value='".$data->barcode."' onclick='plusmin(this,1)'><i class='glyphicon glyphicon-plus'></i></button>".
                    "<input type='number' name='qty' class='form-control col-xs-1' id='qty".$data->barcode."' placeholder='Qty' min='1'>".
                    "<button type='button' class='btn btn-danger btn-sm' value='".$data->barcode."' onclick='plusmin(this,0)'><i class='glyphicon glyphicon-minus'></i></button>".
                    "</form>";
                })
                ->addColumn('action',function($data)
                {
                    return  
                    "<form class='form-horizontal' id='formDelete'>".
                    "<button type='button' class='btn btn-danger btn-sm'  value='".$data->id."' onclick='hapus(this)'><i class='glyphicon glyphicon-trash'></i></button>".
                    "</form>";
                })
                ->rawColumns(['plusmin','action'])
                ->make(true);
    }
    public function scan($kd,$barcode)
    {
        $data = barang::where('barcode','=',$barcode)->first();
        if ($data == null) {
            return response()->json(['status'=>'null']);
        }else{
            $cek = detail_barang_keluar::where(['kd_barang_keluar'=>$kd,'barang_id'=>$data->id]);
            if ($cek->count() > 0) {
                $status = 'update';
                $detail = $cek->first();
                return response()->json(['data'=>$data,'status'=>$status,'detail'=>$detail]);
            }else{
                $status = 'add';
                return response()->json(['data'=>$data,'status'=>$status]);
            }

        }
    }
    public function batal($kd)
    {
        $detail = detail_barang_keluar::where(['kd_barang_keluar'=>$kd]);
        $detail->delete();
        return response()->json(['status'=>"success"]);
    }
}
