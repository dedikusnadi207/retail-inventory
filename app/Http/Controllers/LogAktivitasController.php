<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\log_aktivitas;
use Datatables;
use DB;
class LogAktivitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['menu'=>'logAktivitas']);
        return view('log.aktivitas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aktivitas = log_aktivitas::findOrFail($id);
        $aktivitas->delete();
        $response = array('status' => 'success');
        return response()->json($response);
    }

    public function datatables()
    {
        $aktivitas = log_aktivitas::select(DB::raw('`menu`,MAX(`created_at`) as `waktu`'))
        ->groupBy('menu')
        ->get();
        return Datatables::of($aktivitas)
                ->addColumn('aksi',function($data)
                {
                    $url = url('logAktivitas/datatables/'.$data->menu);
                    return "<button type='button' class='btn btn-primary btn-sm'  value='".$url."' onclick='detailMenu(this)'><i class='glyphicon glyphicon-th-list'></i></button>";
                })
                ->rawColumns(['aksi'])
                ->make(true);
    }

    public function detailMenu($menu)
    {
        $aktivitas = log_aktivitas::join('user','user.id','=','log_aktivitas.user_id')
        ->select(DB::raw('
                log_aktivitas.id,
                user.nama as nama,
                log_aktivitas.aksi as deskripsi,
                (CASE log_aktivitas.aktivitas
                    WHEN "C" THEN "Tambah"
                    WHEN "D" THEN "Hapus"
                    WHEN "U" THEN "Ubah"
                    ELSE "-"
                 END) AS aktivitas,
                 menu,
                 log_aktivitas.created_at as waktu
            '))->where(['menu'=>$menu])->get();

        return Datatables::of($aktivitas)
                ->addColumn('aktivitas',function($data)
                {
                    $label = $data->aktivitas;
                    if ($data->menu == "Tempat Sampah") {
                        if ($label == "Tambah") {
                            $label = "Mengembalikan";
                            $classLabel = "success";
                        }elseif ($label == "Hapus") {
                            $label = "Hapus Permanen";
                            $classLabel = "danger";
                        }
                    }else{
                        switch ($label) {
                            case 'Tambah':
                                $classLabel = "success";
                                break;
                            case 'Ubah':
                                $classLabel = "warning";
                                break;
                            case 'Hapus':
                                $classLabel = "danger";
                                break;
                        }
                    }
                    return "<span class='label label-".$classLabel."'>".$label."</span>";
                })
                ->addColumn('aksi',function($data)
                {
                    return "<button type='button' class='btn btn-primary btn-sm'  value='".$data->id."' onclick='detailLog(this,\"".$data->aktivitas."\")'><i class='glyphicon glyphicon-eye-open'></i></button>";
                })
                ->rawColumns(['aksi','aktivitas'])
                ->make(true);

    }

    public function detailLog($id)
    {
        $datas = log_aktivitas::findOrFail($id);
        return view('log.tablePopUp',compact('datas'));
    }

    public static function simpan($datas,$aksi,$menu,$status)
    {
        $input = [
            'user_id'     => session('userProfile')->id,
            'aksi'        => $aksi,
            'data_new'    => NULL,
            'data_old'    => NULL,
            'data_change' => NULL,
            'menu'        => $menu,
            'aktivitas'   => ""
        ];
        switch ($status) {
            case 1:
                $input['data_new'] = json_encode($datas[0]);
                $input['aktivitas'] = "C";
                break;
            
            case 2:
                $input['data_new'] = json_encode($datas[0]);
                $input['data_old'] = json_encode($datas[1]);
                $input['data_change'] = json_encode($datas[2]);
                $input['aktivitas'] = "U";
                break;
            
            case 3:
                $input['data_old'] = json_encode($datas[0]);
                $input['aktivitas'] = "D";
                break;

            default:
                exit();
                break;
        }
        log_aktivitas::create($input);
    }
}
