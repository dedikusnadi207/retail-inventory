<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\satuan;
use App\Http\Controllers\LogAktivitasController;
use Validator;
use Datatables;
class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index()
    {
        session(['menu'=>'satuan']);
        return view('satuan.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = Validator::make($input,satuan::$rules);
        if ($validation->passes()) {

            $dataNew = array(
                "Nama" => $request->nama
            );
            $log = array($dataNew);
            LogAktivitasController::simpan($log,"Berhasil Menambahkan Satuan ".$request->nama,"Satuan",1);

            
            satuan::create($input);
            $response = array('status' => 'success');
        }else{
            $response = array('status' => 'required' );
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = satuan::findOrFail($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $satuan = satuan::findOrFail($id);
        $validation = Validator::make($input,satuan::$rules);
        if ($validation->passes()) {

            $dataNew = array(
                "Nama" => $request->nama
            );
            $dataOld = array(
                "Nama" => $satuan->nama
            );
            $dataChange = array_diff_assoc($dataNew, $dataOld);
            $log = array($dataNew,$dataOld,$dataChange);
            LogAktivitasController::simpan($log,"Berhasil Mengubah Satuan ".$request->nama,"Satuan",2);

            
            $satuan->update($input);
            $response = array('status' => 'success' );
        }else{
            $response = array('status' => 'required' );
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $satuan = satuan::findOrFail($id);

        $dataOld = array(
            "Nama" => $satuan->nama
        );
        $log = array($dataOld);
        LogAktivitasController::simpan($log,"Berhasil Menghapus Satuan ".$satuan->nama,"Satuan",3);
        
        
        $satuan->delete();
        $response = array('status' => 'success' );
        return response()->json($response);
    }
    public function datatables()
    {
        $satuan = satuan::query();
        return Datatables::of($satuan)
                ->addColumn('action',function($data)
                {
                    return  
                    "<form class='form-horizontal' id='formDelete'>".
                    "<input type='hidden' name='_token' value='".csrf_token()."'>".
                    "<button type='button' class='btn btn-default btn-sm' value='".$data->id."' onclick='edit(this)'><i class='glyphicon glyphicon-edit'></i></button>".
                    "<button type='button' class='btn btn-danger btn-sm'  value='".$data->id."' onclick='hapus(this)'><i class='glyphicon glyphicon-trash'></i></button>".
                    "</form>";
                })->make(true);
    }
}
