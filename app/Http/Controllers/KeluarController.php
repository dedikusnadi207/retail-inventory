<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\barang_keluar;
use App\Model\detail_barang_keluar;
use App\Model\log_aktivitas;
use Validator;
use Datatables;
use DB;
class KeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['menu'=>'keluar']);
        return view('keluar.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = Validator::make($input,barang_keluar::$rules);
        if ($validation->passes()) {
            barang_keluar::create($input);
            // $inputLog = array('username' => session('userProfile')->username, 'aksi' => "Transaksi Barang Keluar '" . $input['kd_barang_keluar'] . "'");
            // log_aktivitas::create($inputLog);
            $response = "success";
        }else{
            $response = "fail";
        }
        return response()->json(['status'=>$response]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function datatables()
    {
        # code...
    }

    public function kode()
    {
        $kode = "KBK".date('Ymd');
        $barang_keluar = DB::select(DB::raw('SELECT RIGHT(kd_barang_keluar,3) as no FROM barang_keluar WHERE LEFT(kd_barang_keluar,11)=:kd ORDER BY no DESC LIMIT 1'),array('kd' => $kode ));
        if ($barang_keluar != null) {
            $barang_keluar = $barang_keluar[0];
            $no = $barang_keluar->no + 1;
            // $no = 1;
        }else{
            $no = 1;
        }

        if ($no < 10) {
            $kode .= "00" . $no;
        }elseif ($no < 100) {
            $kode .= "0" . $no;
        }elseif ($no < 1000) {
            $kode .=  $no;
        }else{
            $kode .= "0001";
        }

        $total = detail_barang_keluar::select(DB::raw('SUM(jumlah) as jumlah'))->where('kd_barang_keluar','=',$kode)->first();
        $total = $total->jumlah;
        return response()->json(['kode'=>$kode, 'total'=>$total]);
    }
}
