<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\supplier;
use App\Http\Controllers\LogAktivitasController;
use Validator;
use Datatables;
use DB;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        session(['menu'=>'supplier']);
        return view('supplier.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validation = Validator::make($input,supplier::$rules);
        if ($validation->passes()) {
            
            $dataNew = array(
                'Kode Supplier' => $request->kd_supplier,
                'Nama' => $request->nama,
                'Nomor Telepon' => $request->no_telp,
                'Email' => $request->email,
                'Alamat' => $request->alamat
            );
            $log = array($dataNew);
            LogAktivitasController::simpan($log,"Berhasil Menambahkan Supplier ".$request->nama,"Supplier",1);
            
            supplier::create($input);
            $response = array('status'=>'success');
        }else{
            $response = array('status'=>'required');
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = supplier::findOrFail($id);
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $supplier = supplier::findOrFail($id);
        $validation = Validator::make($input,supplier::$rules);
        if ($validation->passes()) {
            
            $dataNew = array(
                'Kode Supplier' => $request->kd_supplier,
                'Nama' => $request->nama,
                'Nomor Telepon' => $request->no_telp,
                'Email' => $request->email,
                'Alamat' => $request->alamat
            );
            $dataOld = array(
                'Kode Supplier' => $supplier->kd_supplier,
                'Nama' => $supplier->nama,
                'Nomor Telepon' => $supplier->no_telp,
                'Email' => $supplier->email,
                'Alamat' => $supplier->alamat
            );
            $dataChange = array_diff_assoc($dataNew, $dataOld);
            $log = array($dataNew,$dataOld,$dataChange);
            LogAktivitasController::simpan($log,"Berhasil Mengubah Supplier ".$request->nama,"Supplier",2);
            
            $supplier->update($input);
            $response = array('status' => 'success');
        }else{
            $response = array('status' => 'required');
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = supplier::findOrFail($id);

        $dataOld = array(
            'Kode Supplier' => $supplier->kd_supplier,
            'Nama' => $supplier->nama,
            'Nomor Telepon' => $supplier->no_telp,
            'Email' => $supplier->email,
            'Alamat' => $supplier->alamat
        );
        $log = array($dataOld);
        LogAktivitasController::simpan($log,"Berhasil Menghapus Supplier ".$supplier->nama,"Supplier",3);
        
        $supplier->delete();
        $response = array('status' => 'success');
        return response()->json($response);
    }

    public function datatables()
    {
        $supplier = supplier::query();
        return Datatables::of($supplier)
                ->addColumn('action',function($data)
                {
                    return  
                    "<form class='form-horizontal' id='formDelete'>".
                    "<input type='hidden' name='_token' value='".csrf_token()."'>".
                    "<button type='button' class='btn btn-primary btn-sm' value='".$data->id."' onclick='show(this)'><i class='glyphicon glyphicon-eye-open'></i></button>".
                    "<button type='button' class='btn btn-default btn-sm' value='".$data->id."' onclick='edit(this)'><i class='glyphicon glyphicon-edit'></i></button>".
                    "<button type='button' class='btn btn-danger btn-sm'  value='".$data->id."' onclick='hapus(this)'><i class='glyphicon glyphicon-trash'></i></button>".
                    "</form>";
                })->make(true);
    }
    public function kode()
    {
        $no = supplier::select(DB::raw('RIGHT(`kd_supplier`, 4) as `no`'))->orderBy('no','desc')->withTrashed()->first();
        return response()->json($no);
    }
}
