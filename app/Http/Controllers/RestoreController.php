<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\jenis;
use App\Model\satuan;
use App\Model\merk;
use App\Model\barang;
use App\Model\supplier;
use App\Http\Controllers\LogAktivitasController;
use Datatables;
class RestoreController extends Controller
{
	public function index($status)
	{
		if ($status != '') {
			session(['menu' => 'Restore'.$status]);
			return view('restore.'.$status);
		}
		return;
	}

	public function fungsiCek($value)
	{
		switch ($value) {
			case 'jenis':
				return jenis::onlyTrashed();
				break;
			
			case 'merk':
				return merk::onlyTrashed();
				break;
			
			case 'satuan':
				return satuan::onlyTrashed();
				break;

			case 'barang':
				return barang::onlyTrashed();
				break;

			case 'supplier':
				return supplier::onlyTrashed();
				break;
			
			default:
				return null;
				break;
		}
	}

	public function datatables($status='')
	{
		$data = $this->fungsiCek($status)->get();
		return Datatables::of($data)
				->addColumn('action',function($data)
				{
                	return 
                	"<div class='form-horizontal'>".
                	"<button type='button' class='btn btn-danger btn-sm'  value='".$data->id."' onclick='hapus(this)'><i class='glyphicon glyphicon-trash'></i> Hapus Permanen</button>".
                	"<button type='button' class='btn btn-warning btn-sm'  value='".$data->id."' onclick='restore(this)'><i class='glyphicon glyphicon-export'></i> Kembalikan</button>".
                	"</div>";
				})->make(true);
	}

	public function forceDelete($status,$id)
	{
		$data = $this->fungsiCek($status)->find($id);
		$nama = $data->nama;
		if ($status == "jenis" || $status == "merk" || $status == "satuan" ) {
	        $dataOld = array(
	            "Nama" => $data->nama
	        );
		}elseif ($status == "barang") {
            $dataOld = array(
                'Barcode' => $data->barcode,
                'Nama' => $data->nama,
                'Merk' => merk::findOrFail($data->merk_id)->nama,
                'Jenis' => jenis::findOrFail($data->jenis_id)->nama,
                'Satuan' => satuan::findOrFail($data->satuan_id)->nama,
                'Berat' => $data->berat ,
                'Harga Beli' => $data->harga_beli,
                'Harga Jual' => $data->harga_jual,
                'Stok' => $data->stok
            );
		}elseif ($status == "supplier") {
			$dataOld = array(
                'Kode Supplier' => $data->kd_supplier,
                'Nama' => $data->nama,
                'Nomor Telepon' => $data->no_telp,
                'Email' => $data->email,
                'Alamat' => $data->alamat
            );
		}
		$data->forceDelete();
        $log = array($dataOld);
        LogAktivitasController::simpan($log,"Berhasil Menghapus ".$status." ".$nama." secara permanen","Tempat Sampah",3);
		$response = array('status' => 'success');
		return response()->json($response);
	}

	public function restore($status,$id)
	{
		$data = $this->fungsiCek($status)->find($id);
		$nama = $data->nama; 
		if ($status == "jenis" || $status == "merk" || $status == "satuan" ) {
			$dataNew = array(
                "Nama" => $data->nama
            );
		}elseif ($status == "barang") {
            $dataNew = array(
                'Barcode' => $data->barcode,
                'Nama' => $data->nama,
                'Merk' => merk::findOrFail($data->merk_id)->nama,
                'Jenis' => jenis::findOrFail($data->jenis_id)->nama,
                'Satuan' => satuan::findOrFail($data->satuan_id)->nama,
                'Berat' => $data->berat ,
                'Harga Beli' => $data->harga_beli,
                'Harga Jual' => $data->harga_jual,
                'Stok' => $data->stok
            );
		}elseif ($status == "supplier") {
			$dataNew = array(
                'Kode Supplier' => $data->kd_supplier,
                'Nama' => $data->nama,
                'Nomor Telepon' => $data->no_telp,
                'Email' => $data->email,
                'Alamat' => $data->alamat
            );
		}
		$data->restore();
        $log = array($dataNew);
        LogAktivitasController::simpan($log,"Berhasil Mengembalikan ".$status." ".$nama,"Tempat Sampah",1);
		$response = array('status' => 'success');
		return response()->json($response);
	}
}
