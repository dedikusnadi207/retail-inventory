<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
class checkLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if (!$request->session()->has('userProfile')) {
            return new Response(view('login'));
        }
        return $response;
    }
}
