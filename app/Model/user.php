<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class user extends Model
{
	public $table = 'user';
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	protected $primary = 'id';
	public $timestamps = true;
	public $fillable = [
		'username',
		'password',
		'nama',
		'no_telp',
		'email',
		'alamat'
	];
	public static $rules = [
		'username' => 'required',
		'password' => 'required',
		'nama'	 => 'required',
		'no_telp' => 'required',
		'email' => 'required|email',
		'alamat'  => 'required'
	];
}
