<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class barang_keluar extends Model
{
	public $table = "barang_keluar";
	protected $primary = "id";
	const CREATED_AT = "created_at";
	const UPDATED_AT = "updated_at";
	public $timestamps = true;
	public $fillable = [
		'kd_barang_keluar',
		'tanggal',
		'total_keluar'
	];
	public static $rules = [
		'kd_barang_keluar' => 'required',
		'tanggal'  => 'required',
		'total_keluar'  => 'required'
	
	];

}
