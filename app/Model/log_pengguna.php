<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class log_pengguna extends Model
{
	public $table = "log_pengguna";
	const CREATED_AT = "logged_in";
	const UPDATED_AT = "logged_out";
	protected $primary = "id";
	public $timestamps = true;
	public $fillable = [
		'username',
		'status'
	];
}
