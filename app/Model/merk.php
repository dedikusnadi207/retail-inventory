<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class merk extends Model
{
	use SoftDeletes;
	public $table = "merk";
	const CREATED_AT = "created_at";
	const UPDATED_AT = "updated_at";
	protected $primary = "id";
	public $timestamps = true;
	protected $dates = ["deleted_at"];
	public $fillable = [
		'nama'
	];
	public static $rules = [
		'nama'=>'required'
	];
}
