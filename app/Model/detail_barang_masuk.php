<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class detail_barang_masuk extends Model
{
	public $table = "detail_barang_masuk";
	protected $primary = "id";
	public $timestamps = false;
	public $fillable = [
		'kd_barang_masuk',
		'barang_id',
		'jumlah'
	];
	public static $rules = [
		'kd_barang_masuk' => 'required',
		'barang_id' => 'required',
		'jumlah'  => 'required'
	];

	public function barang()
	{
		$this->belongsTo('App\Model\barang','barang_id','id');
	}
}
