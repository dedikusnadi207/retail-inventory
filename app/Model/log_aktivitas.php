<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class log_aktivitas extends Model
{
	public $table = "log_aktivitas";
	protected $primary = "id";
	public $timestamps = true;
	public $fillable = [
		'user_id',
		'data_new',
		'data_old',
		'data_change',
		'aksi',
		'menu',
		'aktivitas'
	];

	public function user()
	{
		return $this->belongsTo('App\Model\user','user_id','id');
	}
}
