<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class supplier extends Model
{
	use SoftDeletes;
	public $table = "supplier";
	const CREATED_AT = "created_at";
	const UPDATED_AT = "updated_at";
	protected $primary = "id";
	public $timestamps = true;
	protected $dates = ['deleted_at'];
	public $fillable = [
		'kd_supplier',
		'nama',
		'no_telp',
		'email',
		'alamat'
	];
	public static $rules = [
		'kd_supplier'=>'required',
		'nama'=>'required',
		'no_telp'=>'required',
		'email'=>'required|email',
		'alamat'=>'required'
	];
}
