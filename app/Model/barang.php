<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class barang extends Model
{
	use SoftDeletes;
	public $table = "barang";
	const CREATED_AT = "created_at";
	const UPDATED_AT = "updated_at";
	protected $primary = "id";
	public $timestamps = true;
	protected $dates = ['deleted_at'];
	public $fillable = [
		'barcode',
		'nama',
		'merk_id',
		'jenis_id',
		'satuan_id',
		'berat',
		'harga_beli',
		'harga_jual',
		'stok'
	];
	public static $rules = [
		'barcode' => 'required',
		'nama' => 'required',
		'merk_id' => 'required',
		'jenis_id' => 'required',
		'satuan_id' => 'required',
		'berat' => 'required',
		'harga_beli' => 'required',
		'harga_jual' => 'required',
		'stok' => 'required'
	];

	public function merk()
	{
		return $this->belongsTo('App\Model\merk','merk_id','id');
	}
	public function jenis()
	{
		return $this->belongsTo('App\Model\jenis','jenis_id','id');
	}
	public function satuan()
	{
		return $this->belongsTo('App\Model\satuan','satuan_id','id');
	}
}
