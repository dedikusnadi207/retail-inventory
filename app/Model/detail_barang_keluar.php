<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class detail_barang_keluar extends Model
{
	public $table = "detail_barang_keluar";
	protected $primary = "id";
	public $timestamps = false;
	public $fillable = [
		'kd_barang_keluar',
		'barang_id',
		'jumlah'
	];
	public static $rules = [
		'kd_barang_keluar' => 'required',
		'barang_id' => 'required',
		'jumlah'  => 'required'
	];

	public function barang()
	{
		$this->belongsTo('App\Model\barang','barang_id','id');
	}
}
