<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class barang_masuk extends Model
{
	public $table = "barang_masuk";
	protected $primary = "id";
	const CREATED_AT = "created_at";
	const UPDATED_AT = "updated_at";
	public $timestamps = true;
	public $fillable = [
		'kd_barang_masuk',
		'tanggal',
		'supplier_id',
		'total_masuk'
	];
	public static $rules = [
		'kd_barang_masuk' => 'required',
		'tanggal'  => 'required',
		'supplier_id'  => 'required',
		'total_masuk'  => 'required'
	
	];

	public function supplier()
	{
		$this->belongsTo('App\Model\supplier','supplier_id','id');
	}
}
